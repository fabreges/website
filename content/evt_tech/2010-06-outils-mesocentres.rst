Mésocentres : quels outils pour aujourd'hui et pour demain ?
#############################################################

:date: 2010-02-01 19:41:01
:category: journee
:tags: mesocentres
:authors: Romaric David
:place: Lyon
:start_date: 2010-06-10
:end_date: 2010-06-10

.. contents::

.. section:: Contexte
  :class: description

  Depuis quelques temps, la communauté des mésocentres se réunit
  régulièrement sur des aspects scientifiques, techniques ou politiques,
  comme ce fut le cas le 24 Septembre 2009 à Paris.

  Il nous paraît important de favoriser la mise en relation dans tous les
  aspects des métiers liés aux mésocentres, y compris au niveau opérationnel.

  Pour cette raison, le département Expertise pour la Recherche de la
  Direction Informatique de l'Université de Strasbourg et le Groupe Calcul
  du Cnrs ont organisé le 10 Juin 2010 au CC IN2P3 à Lyon une journée
  technique sur les outils utilisés dans les mésocentres.

  Durant cette journée, des responsables techniques de mésocentres ont
  présenté les outils qu'ils utilisent, aussi bien en terme de déploiement
  de systèmes, de gestion de batch, que d'outils de développement.

  Les orateurs ont abordé les problèmes rencontrés, les éventuelles
  limitations observées, et ont expliqué le rôle joué par des prestataires
  extérieurs selon le cas.

  `Plaquette de la journée <attachments/spip/Documents/Journees/juin2010/LyonCalcul.pdf>`__

  Cette manifestation bénéficie du soutien de CIRA
  et de `GENCI <http://www.genci.fr/>`__.

.. section:: Programme et supports
  :class: programme

  .. schedule::

    .. day:: 10-06-2010

      .. event:: Introduction à la journée
        :begin: 13:30
        :end: 13:40
        :speaker: R. David, V. Louvet
        :support: attachments/spip/Documents/Journees/juin2010/rdavid.pdf

      .. event:: L'expérience d'un jeune mésocentre
        :begin: 13:40
        :end: 14:10
        :speaker: Laurent Philippe, Méso-Comté
        :support: attachments/spip/Documents/Journees/juin2010/meso-comte.pdf

        L'expérience d'un jeune mésocentre
        Le mésocentre de calcul de Franche-Comté a été créé il y a un peu plus
        d'un an et son cluster est en production depuis le mois de novembre
        2009. Comme aucune structure ne préexistait, les choix matériels et
        logiciels n'ont pas été soumis à des contraintes de cohérence par
        rapport Ã  une base en place mais plutÃ´t Ã  une garantie de mise en
        place rapide par une équipe inexpérimentée. De même, si un certain
        nombre d'utilisateurs pratiquaient déjà le calcul intensif hors des
        ressources de l'université, il a fallu les convaincre d'utiliser les
        ressources locales et étendre cette base vers des utilisateurs
        inexpériementés.

        Notre exposé fera le point sur l'avancée du projet,
        les choix réalisés, la configuration en place et les difficultés
        rencontrés. Nous ferons également un premier bilan de la satisfaction
        des utilisateurs avant de terminer par des perspectives.

      .. event:: Expériences autour de OAR
        :begin: 14:10
        :end: 14:40
        :speaker: Françoise Roch, Observatoire de Grenoble
        :support: attachments/spip/Documents/Journees/juin2010/osug.pdf

        L'Observatoire de Grenoble est une structure qui fédère différents
        laboratoires des sciences de l'Univers. Son centre de calcul intensif
        est commun et compte une centaine d'utilisateurs.

        Après avoir expérimenté, dans le passé, différents gestionnaires de
        batch public domain ou constructeur, nous avons choisi en 2004 de jouer
        le rôle de beta-testeur pour le gestionnaire OAR développé par nos
        collègues grenoblois du laboratoire d'Informatique Distribué
        (également partenaire dans le projet CIMENT).

        Cette expérience s'est accompagnée d'une collaboration étroite autour
        de OAR, avec les concepteurs du logiciel, tout au long de son
        développement. Ce partenariat a été très enrichissant :

        - tant du point de vue des informaticiens qui ont pu valider leur
           logiciel sur des plateformes de taille réaliste avec un large spectre
           d'applications,
        - que du point de vue des utilisateurs qui ont pu exprimer leurs
           besoins, et pu bénéficier en retour de nouvelles fonctionalités
           grâce à l'expertise d'une équipe de recherche ayant à la fois une
           compétence sur les outils pour cluster et grilles, et sur l'ordonnancement.

        Je décrirai nos diverses utilisations du logiciel OAR, dans
        différents contextes; ses avantages et inconvénients du point de vue
        administrateur et du point de vue utilisateur.

      .. event:: Le mésocentre hébergé à l'O.C.A.
        :begin: 14:40
        :end: 15:10
        :speaker: Alain Miniussi, Observatoire de la Cote d'Azur
        :support: attachments/spip/Documents/Journees/juin2010/oca-small.pdf

        Le mesocentre de calcul intensif hébergé à l'Observatoire de la Côte d'Azur (www.oca.eu) a pour mission principale de répondre à une certaine gamme de besoins HPC de l'ensemble des laboratoires de l'Université de Nice Sophia Antipolis (www.unice.fr) et de l'OCA qui en font la demande, et ce indépendamment de leurs thèmes de recherches. Cela implique deux contraintes principales :

        - l'absence de profil type nous a amené  à  acquérir des plates-formes « tout terrain »
        - le nombre et la répartition géographique des utilisateurs nous obligent à passer dans la mesure du possible par un support à distance.

        Nous présentons tous d'abord notre processus d'acquisition et son articulation par rapport aux utilisateurs, aux laboratoires partenaires et aux fournisseurs. Nous décrivons également notre mode de fonctionnement : critère d'éligibilité, partage des ressources, support, formations et arbitrage.

        Enfin, la question de la mutualisation prend une place grandissante. En terme de moyens humains, nous avons choisi de nous intégrer autant que faire se peut avec le service informatique de l'OCA afin, notamment, d'éviter les redondances. De l'autre coté, une mutualisation des moyens de calculs et de stockage peut se révéler bénéfique à la fois pour les utilisateurs, qui tirent partie des économies d'échelle, et pour les équipes techniques. Cependant, la mise en commun de ces moyens suscite parfois encore des réticences qu'il nous appartient de gérer.

      .. event:: Gestion de clusters de grande taille
        :begin: 15:10
        :end: 15:40
        :speaker: Matthieu Hautreux, CEA
        :support: attachments/spip/Documents/Journees/juin2010/cea.pdf

        Compte tenu des dernières évolutions dans les architectures
        processeurs et face au besoin de performances exprimé par la
        communauté des utilisateurs, les grappes de calcul généralistes sont
        confrontées à une explosion du nombre d'éléments. Cette augmentation
        considérable du volume de composants induit de nouvelles contraintes
        sur la manière d'exploiter au mieux de telles ressources. Que ce soit
        pour des problématiques de déploiement, de gestion des configurations,
        d'administration ou bien encore d'utilisation, la notion d'échelle ne
        peut plus être oubliée. Les milliers de nœuds de calculs désormais
        nécessaires doivent être abstraits, simplifiés, rationalisés afin de
        formaliser les besoins en une quantité raisonnable d'objets facilement
        appréhendables, manipulables et réplicables.
        Cet exposé décrit les principes de fonctionnement et d'utilisation
        d'outils adaptés ou adaptables à ces problématiques, comme le sont
        Yum, SystemImager, Puppet, ClusterShell ou encore Slurm.

      .. break_event:: Pause
        :begin: 15:40
        :end: 16:10

      .. event:: Interfaçage mésocentre / grille
        :begin: 16:10
        :end: 16:40
        :speaker: Éric Fede, Lapp, Annecy
        :support: attachments/spip/Documents/Journees/juin2010/lapp.pdf

        Le mésocentre de l'Université de Savoie (MUST) présente comme caractéristique particulière d'être ouverts sur la grille de calcul Européenne EGEE. Ainsi cette infrastructure est à la fois sollicitée par des utilisateurs « grille », universitaires  et « locaux ».

        Je me propose dans cette présentation de mettre en avant les mécanismes qui permettent de gérer ces trois familles d’utilisateurs, mais aussi les différents types de taches auxquelles on doit faire face (jobs séquentiels, jobs parallèles, logiciels à licences,…).

        Enfin je présenterai quelques uns des choix d’outils que  nous avons faits pour remplir toutes ces contraintes, notamment au niveau de l’outil de déploiement/configuration des machines du mésocentre et de l’accounting.

      .. event:: Mésocentre de bourgogne, évolutions techniques
        :begin: 16:40
        :end: 17:10
        :speaker: Jean-Jacques Gaillard, Didier Rebeix, Olivier Politano, Dijon
        :support: attachments/spip/Documents/Journees/juin2010/univ-bourgogne.pdf

        Les grands problèmes à régler : alimentation électrique, climatisation, stockage haute performance

        - Didier Rebeix   MPI --> quel MPI
        - migration LSF SGE : intégration SGE/MPI/OPENMP
        - un métascheduler ?
        - Olivier Politano  Quel compilateur? GNU, Portland, Intel?
        - précision et erreur d'arrondi
        - compatibilité AMD/Intel
        - compromis sur les performances

      .. event:: Ordonnancement au CC IN2P3
        :begin: 17:10
        :end: 17:40
        :speaker: Suzanne Poulinat
        :support: attachments/spip/Documents/Journees/juin2010/in2p3.pdf

        - Ordonnancement au CC IN2P3
        - Présentation du CC
        - Organisation du service de batch afin d'optimiser l'utilisation des ressources

      .. event:: Évolution des outils, besoins et formations  Discussion ouverte
        :begin: 18:00
        :end: 19:00

.. section:: Organisation
  :class: orga

  - Romaric David
  - Violaine Louvet

