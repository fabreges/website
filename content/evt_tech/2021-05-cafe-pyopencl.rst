GPU programming with PyOpenCL
#############################

:date: 2021-05-20 10:00:00
:category: cafe
:tags: visio
:start_date: 2021-05-20 10:00:00
:end_date: 2021-05-20
:place: En ligne
:summary: Calcul parallèle avec PyOpenCL.
:inscription_link: https://indico.mathrice.fr/event/255
:calendar_link: https://indico.mathrice.fr/export/event/255.ics

.. contents::

.. section:: Description
    :class: description

    Ce Café sera réalisé par Jérôme Kieffer, membre de l'Installation européenne de rayonnement synchrotron (ESRF).

    Cette présentation porte sur la programmation parallèle en utilisant PyOpenCL. Elle sera réalisée sous forme de notebooks jupyter ce qui permet de faire des démonstrations en direct.

    Contenu :

    Présentation générale des GPU et pourquoi ils ne sont pas si différents des CPU modernes
    Les plateformes OpenCL vu à travers PyOpenCL
    Une petite comparaison avec Numba, Numba-cuda et Julia
    Premier kernel, exemple d'addition de vecteurs
    Les patrons de conceptions en programmation parallèle
    La meta-programmation avec PyOpenCL.

    Cette session est accessible à tous et aura lieu sur la plateforme BBB de Mathrice.

    Merci de bien vouloir vous inscrire pour suivre cette session. Cette session sera enregistrée. S'inscrire implique d'accepter ce principe.

.. button:: Supports
    :target: https://github.com/silx-kit/silx-training/blob/master/opencl/OpenCL.ipynb

.. button:: Vidéo sur canal-u.tv
    :target: https://www.canal-u.tv/video/groupe_calcul/gpu_programming_with_pyopencl.61533

.. section:: Orateur
    :class: orateur

      - Jérôme Kieffer (`ESRF <https://www.esrf.eu/>`_)
      
