Journées GPGPU
##############

:date: 2018-05-08 11:44:31
:category: journee
:authors: Anne Cadiou
:start_date: 2018-06-12
:end_date: 2018-06-13
:place: Villeurbanne
:summary: Journées thématiques sur le calcul sur GPGPU.

.. contents::

.. section:: Description
    :class: description

    Les architectures dites GPGPU (General-purpose processing on graphics processing units) utilisent le traitement nativement parallèle des processeurs graphiques pour accélérer les calculs. L'essor récent de ces technologies rend les architectures GPGPU compétitives avec les solutions parallèles usuelles à base de CPU en terme de temps de calcul, pour des coûts bien inférieurs.

    Malgré un paradigme de programmation totalement différent, issu des différents niveaux de mémoire des cartes graphiques, une seule carte actuelle peut atteindre des performances théoriques de l'ordre de 1 000 GFLOPs sur des calculs en simple précision, ayant des temps d'accès mémoire environ 5 fois plus rapides que les architectures CPU classiques.

    Les solutions actuelles en calcul intensif et en visualisation se tournent de plus en plus vers les architectures hybrides, offrant une plus grande capacité de calcul aux applications de la physique (dynamique moléculaire, mécanique ...) ainsi qu'aux apprentissages automatiques (traitement d'images, bio-informatique ...).

    Les différents niveaux de programmation possibles (bibliothèques, directives, langages dédiés, ...) rendent ce type de calcul plus accessible à tous les niveaux de la physique numérique.

    L'objectif de ces journées est de faire un point sur les architectures actuelles, les languages de programmation et runtime associés.

    **Lieu**
    Amphi Thémis 10, Campus de la Doua, Villeurbanne

    Accès amphi 10 du bâtiment Thémis : À l'angle de l'avenue Gaston Berger et de l'Avenue des Arts sur le Campus de la Doua
    https://www.univ-lyon1.fr/campus/plan-des-campus/batiment-themis-776709.kjsp

    GPS : 45.78327, 4.87156

    Itinéraire en tram :
    - Prendre le tram T1, direction, IUT Feyssine ou le T4 direction la Doua - Gaston Berger
    - Descendre à l'arrêt Gaston Berger

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 12-06-2018

            .. event:: introduction aux architectures et langagesIntroduction
                :begin: 09:00
                :end: 12:00
                :speaker: Jonathan Rouzaud-Cornabas (LIRIS, INRIA)
                :support:
                    [cours_1_intro.pdf](attachments/spip/IMG/pdf/cours_1_intro.pdf)
                    [cours_2_archi.pdf](attachments/spip/IMG/pdf/cours_2_archi.pdf)
                    [cours_gpu_1.pdf](attachments/spip/IMG/pdf/cours_gpu_1.pdf)
                    [cours_gpu_2.pdf](attachments/spip/IMG/pdf/cours_gpu_2.pdf)
                    [cours_gpu_3.pdf](attachments/spip/IMG/pdf/cours_gpu_3.pdf)


            .. event:: introduction aux architectures et langagesIntroduction
                :begin: 14:00
                :end: 17:00
                :speaker: Thierry Gautier (LIP, INRIA)
                :support: attachments/spip/IMG/pdf/2018_slide_lyoncalcul.pdf

        .. day:: 13-06-2018

            .. event:: applications pour la physique et analyse de performance
                :begin: 09:00
                :end: 12:00
                :speaker: Emmanuel Quemener(Centre Blaise Pascal, ENS Lyon)
                :support: http://www.cbp.ens-lyon.fr/emmanuel.quemener/documents/LyonCalcul_GPU_180613_EQ.pdf

            .. event:: retour d'expérience de la cellule de veille technologique de GENCI
                :begin: 09:00
                :end: 12:00
                :speaker: Gabriel Hautreux (GENCI)
                :support: attachments/spip/IMG/pdf/presgenci-gpgpu-lyon-2018.pdf
