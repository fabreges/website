École "Programmation hybride : une étape vers le many-coeurs ?"
###############################################################

:date: 2012-09-27 11:24:10
:category: formation
:place: Autrans
:start_date: 2012-10-08
:end_date: 2012-10-12


.. contents::

.. section:: Descriptif de la formation
  :class: description

  Lieu : `Centre de l'Escandille <http://www.escandille.com>`_, Autrans, Isère

  Les performances des moyens de calcul mis à disposition des chercheurs ne cessent de croître que ce soit au niveau régional avec les mésocentres, au niveau national avec GENCI ou au niveau européen avec le projet PRACE. Ces ressources ont pour point commun d’être **hybrides** , c’est-à-dire de **combiner fortement plusieurs technologies différentes** , ce qui induit une complexité accrue d’utilisation et peut entraîner des freins à leur adoption.

  Or, le calcul hybride est devenu un passage incontournable pour exploiter pleinement les ressources de calcul d’aujourd’hui et de demain. En effet, les systèmes de calcul se présentent comme des architectures massivement parallèles et hétérogènes. Ils sont basés sur des ensembles de nœuds à architecture mémoire non‐uniforme (NUMA) interconnectés par des réseaux rapides, chaque nœud s'organisant autour d'un ensemble d'unités de calcul hétérogène: carte multi‐cœurs, processeurs graphiques (GPU) ou d'autres types de carte accélératrice. Les bibliothèques classiques bien connues comme MPI ou OpenMP ne suffisent donc plus seules à exploiter pleinement ce type d'architecture.

  Cette formation a pour but de donner les **grandes tendances du calcul haute performance dans les prochaines années**  et de donner aux participants des bases solides leur permettant de tirer profit des nouvelles architectures globales de calcul.

  Nous nous intéresserons à un problème concret qui sera**hybridé à l'aide de MPI, openMP et GPU** . Nous parlerons ensuite du placement des threads et de l'utilisation de plusieurs GPU. Enfin, nous aborderons 2 runtime sur notre problème modèle: openHMPP et MPC.


.. section:: Programme
  :class: programme

  .. schedule::

    .. day:: 08-10-2012

      .. event:: Pourquoi l’hybride ?
        :begin: 9:00
        :end: 12:30
        :speaker: E. Petit (Université Versailles St Quentin)
        :support: attachments/spip/IMG/pdf/presentation-Autrans-introduction.pdf

        Au milieu de la dernière décennie, les architectures des processeurs ont pris un  très net virage. Pour des raisons de consommation, de surface, de complexité et d'efficacité, la progression des architectures mono-coeurs a ralenti puis a laissé la place aux architectures multi-coeurs. Afin d'en obtenir les meilleures performances, il est nécessaire de faire apparaître du parallélisme de tâches. Dans le HPC cela se traduit par  l'utilisation de schémas de programmation hybride tel que MPI+OpenMP.
        Par ailleurs, parmi les pistes explorées pour contourner le ralentissement des architectures mono-coeurs, des solutions d'accélérateurs matériels ont été étudiées. Les efforts de compatibilité aux normes (flottant en particulier), les efforts sur les environnements de programmation et le support d'un marché grand public ont permis aux GPUs de s'imposer, en particulier le couple NVIDIA/CUDA. Leurs architectures massivement parallèles présentent un paradigme nouveau pour les développeurs, les obligeant à retravailler en profondeur leurs applications.
        Dans ce cours, nous allons aborder les mécanismes architecturaux de base permettant de comprendre l'apport des multi-coeurs et les différences fondamentales entre CPU et GPU. Nous verrons ensuite comment et pourquoi ces architectures impactent les applications. Enfin nous aborderons les architectures nouvelles et les perspectives pour l'avenir.

      .. event:: Un exemple de A à Z
        :begin: 14:00
        :end: 15:30
        :speaker: M. Tajchman
        :support: attachments/spip/IMG/tgz/supporttajchman.tgz

        Le cours commencera par une introduction sur les méthodes physiques,
        numériques et algorithmiques mises en place dans un code 2D hydrodynamique.
        On présentera ensuite des versions construites en utilisant différentes
        méthodes de parallélisation: multi-threads (OpenMP, TBB), multi-processus
        (MPI), calcul sur GPU (Cuda), ainsi que des combinaisons de ces techniques.
        L'examen de ces versions ainsi que des exemples simples illustreront des
        conseils sur les choix de parallélisation, l'importance de l'organisation
        des accès mémoire, le recouvrement calcul-communications, etc.

    .. day:: 09-10-2012

      .. event:: Un exemple de A à Z(suite)
        :begin: 9:00
        :end: 17:30

    .. day:: 10-10-2012

      .. event:: Placement des processus
        :begin: 09:00
        :end: 12:30
        :speaker: L. Saugé (BULL)
        :support: attachments/spip/IMG/gz/placementThreads-tar.gz

        Le caractère "fortement" NUMA des architectures many/multi cores en général, nous oblige à nous poser la question de la stratégie du placement des processus dans ces environnements.
        En effet, dans ce contexte, l'un des premiers principes d'optimisation (de la performance) des codes est d'assurer aux processus la localité des accès mémoires et autres ressources.

        Le but de ce cours est de d'appréhender et comprendre les contraintes qu'impose ces architecture sur les performances des codes "parallèles" au sens large, que ce soit en mode intra ou inter-noeuds.

        Il sera fait quelques rappels sur les architectures des machines multi/many cores (cpu, mémoires) et il sera abordé en détails les moyens d'assurer le placement des processus afin d'optimiser les performances des codes.

        Divers aspects pratiques seront abordés, entre autre:

      .. event:: Parallel Codes and High Performance Computing: Massively parallelism and Multi-GPU
        :begin: 14:00
        :end: 17:30
        :speaker: L. Genovese (CEA)
        :support: attachments/spip/IMG/pdf/HybridCo.pdf

        In this presentation, we will start by revisiting the main aspects of evolution of parallel architectures from three points of view:
        Machine construction, code developments and code usage.
        In particular, we will see how these facts will influence the conception of High Performance Computing codes.
        We will focus on the aspects of combining CPU and GPU acceleration in a massively parallel environments.

        In particular, we will see how hybrid codes conceptions influence the interpretation of performance results by the user.
        We will provide some examples which will show that performance benchmarking is a multicriterion evaluation process: the application should be correctly dimensioned on the computing machine.

    .. day:: 11-10-2012

      .. event:: MPC
        :begin: 09:00
        :end: 12:30
        :speaker: M. Pérache et S. Valat (CEA)
        :support: attachments/spip/IMG/pdf/Ecole_hybride.pdf

        L'évolution actuelle des architectures vers le multi/many-core marque la fin du modèle Full-MPI au profit d'une approche MPI + X (X étant un modèle de threads/tâches). Dans ce contexte, le CEA et le laboratoire Exascale ont développé le support exécutif MPC qui a pour objectif d'unifier les modèles de programmation MPI et OpenMP/PThread au sein d'un même runtime. Cette unification permet d'optimiser l'implémentation MPI pour tirer parti de la présence d'autres threads et vis-versa pour OpenMP. Dans le même temps, l'arrivée des multi-cœurs a pour effet d'amorcer la diminution de la quantité de mémoire disponible par cœur. Dans ce contexte, MPC propose des extensions de gestion des données (HLS, allocateur mémoire, ...) pour les "legacy codes" qui permettent de réduire l'empreinte mémoire du support exécutif, mais aussi du code utilisateur en factorisant les données entre les processus MPI par exemple.


      .. event:: Programmation par directives pour le calcul hybride: HMPP et OpenACC
        :begin: 14:00
        :end: 17:30
        :speaker: F. Lebeau (CAPS Entreprise)
        :support: attachments/spip/IMG/gz/EcoleHybride-tar.gz

        Cette présentation fournira aux participants les bases et la méthodologie pour porter une application vers les systèmes hétérogènes. Après une introduction aux architectures massivement parallèles, les participants apprendront à optimiser efficacement une application avec les jeux de directives HMPP et OpenACC : notamment en ce qui concerne les transferts de données et les optimisations de calculs. Cette présentation s'accompagnera d'une description du fonctionnement d'HMPP Workbench développé par CAPS entreprise et de ses fonctionnalités.

    .. day:: 12-10-2012

      .. event:: Programmation par directives pour le calcul hybride: HMPP et OpenACC (suite)
        :begin: 09:00
        :end: 14:00
