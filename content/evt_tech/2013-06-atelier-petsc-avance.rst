Atelier "PETSc avancé"
######################

:date: 2014-11-05 15:53:49
:category: formation
:start_date: 2013-06-11
:end_date: 2013-06-13
:place: Maison de la Simulation, Saclay
:tags: petsc
:summary: Atelier sur l'utilisation avancée de la librairie PETSc.


.. contents::

.. section:: Description
    :class: description

    Le Groupe Calcul en collaboration avec la Maison de la Simulation (MdS) accueilleront Matthew Knepley (un des développeurs de PETSc) pour une session avancée concernant l'utilisation de la librairie PETSc. Cette formation aura lieu du 11 au 13 juin 2013 dans les locaux de la MdS et sera en anglais.

    Les supports sont disponibles `ici <attachments/spip/IMG/pdf/ParisTutorial.pdf>`__

    Quelques `indications <http://www.maisondelasimulation.fr/Phocea/Page/index.php?id=62>`__ pour accéder à la Maison de la Simulation.

    Les points abordés lors de ces trois jours de formation seront :

    - **Interaction with Mesh and Discretization**

      - DM

        - Structured (DMDA)
        - Unstructured (DMPlex)

      - Managing discretized data

        - FEM
        - FVM (polyhedral)
        - Multifield

    - **Advanced Solvers**

      - Multigrid
      - FieldSplit

        - Saddle-point systems

      - Nonlinear solvers

        - Nonlinear preconditioners (Anderson, NGMRES, NASM)

      - Timestepping

        - IMEX
