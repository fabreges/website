Journée précision numérique
###########################

:date: 2019-06-13
:category: journee
:tags: précsion numérique, hpc, outils
:start_date: 2019-06-28
:end_date: 2019-06-28
:place: Maison de la Simulation
:inscription_link: https://indico.mathrice.fr/event/166/registration


.. contents::

.. section:: Description
    :class: description

    Erreurs d’arrondis, précision et bruit numériques sont des problèmes réccurents rencontrés dans les codes de calcul. Si la validation numérique de l’implémentation d’une méthode numérique donnée est relativement bien connue, la vérification numérique de l’impact de la précision numérique sur le solveur est moins systématique. La place croissante utilisée par les calculateurs intégrant des GPU, pose la question de la pertinence de réduire la précision des nombres à virgule flottante utilisés pour la calcul, sans dégrader la solution numérique. Cela ouvre la possibilité d’ajuster la précision avec laquelle les différentes étapes du calcul sont effectuées. Une condition nécessaire à cette amélioration est la connaissance de la précision nécessaire dans ces différentes parties des algorithmes.
    
    **Objectif :**
    
    Il s’agit de faire un point sur des méthodes et outils permettant d’analyser l’impact de la précision numérique sur la fiabilité des résultats obtenus par les codes. La journée sera composée d’exposés théoriques (approches probabilistes et par arithmétique d'intervalles), de présentation d’outils (CADNA, Verificarlo, VERROU et Fluctuat) et de retours d’expérience de leur utilisation sur des codes applicatifs.
    
    **Intervenants :**
    
    * Fabienne Jézéquel (LIP)
    * Julien Brajard (UPMC)
    * Matthieu Haefele (MdlS)
    * François Févotte (EDF)
    * Éric Petit (Intel)
    * Peter Dueben (ECMWF)
    * Hadrien Grasland (Lal)
    * Bruno Lathuilière (EDF)
    * Franck Védrine (CEA)
    
    Indications pour venir à la `Maison de la Simulation <http://www.maisondelasimulation.fr/venir-chez-nous>`_
    
    Date limite d'inscription : **17/06/2019**


    Nombre de participants : 35

.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 28-06-2019
                
                .. break_event:: Accueil café
                    :begin: 09:30
                    :end: 10:00
        
                
                
                .. event:: Arithmétique stochastique synchrone et CADNA
                    :begin: 10:00
                    :end: 10:45
                    :speaker: Fabienne Jézéquel
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support00.pdf)
        
                    
                    
                
                .. event:: CADNA for simulation and data assimilation: a user perspective
                    :begin: 10:45
                    :end: 11:05
                    :speaker: Julien Brajard
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support01.pdf)
        
                    
                    
                
                .. event:: Conjugate gradient analysis with CADNA for an efficient FPGA implementation
                    :begin: 11:05
                    :end: 11:25
                    :speaker: Matthieu Haefele
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support02.tgz)
        
                    
                    
                
                .. break_event:: Pause
                    :begin: 11:25
                    :end: 11:40
        
                
                
                .. event:: Asynchronous Stochastic Arithmetic
                    :begin: 11:40
                    :end: 12:05
                    :speaker: Eric Petit
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support03.pdf)
        
                    
                    
                
                .. event:: Verificarlo: Debugging and optimizing floating point usage in numerical simulations
                    :begin: 12:05
                    :end: 12:25
                    :speaker: Eric Petit
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support04.pdf)
        
                    
                    
                
                .. event:: A detailed precision analysis for weather and climate models with Verificarlo
                    :begin: 12:25
                    :end: 12:45
                    :speaker: Peter Dueben
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support05.pdf)
        
                    
                    
                
                .. break_event:: Pause déjeuner
                    :begin: 12:45
                    :end: 13:55
        
                
                
                .. event:: Groupe de Travail ARITH du GdR Informatique Mathématique
                    :begin: 13:55
                    :end: 14:00
                    :speaker: Sylvie Boldo
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support06.pdf)
        
                    
                    
                
                .. event:: Verrou : déboguage numérique des codes de calcul industriels
                    :begin: 14:00
                    :end: 14:20
                    :speaker: François Fevotte
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support07.pdf)
        
                    
                    
                
                .. event:: Confidence intervals for stochastic arithmetic
                    :begin: 14:20
                    :end: 14:40
                    :speaker: François Fevotte
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support08.pdf)
        
                    
                    
                
                .. event:: Floating-point profiling of ACTS with Verrou
                    :begin: 14:40
                    :end: 15:00
                    :speaker: Hadrien Grasland
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support09.pdf)
        
                    
                    
                
                .. break_event:: Pause
                    :begin: 15:00
                    :end: 15:30
        
                
                
                .. event:: Propagation of rounding errors by interval arithmetic and affine forms
                    :begin: 15:30
                    :end: 16:15
                    :speaker: Franck Vedrine
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support10.pdf)
        
                    
                    
                
                .. event:: Fluctuat: Verification of accuracy properties of numerical components and synchronous embedded software
                    :begin: 16:15
                    :end: 16:35
                    :speaker: Franck Vedrine
                    :support:
                        [support 1](attachments/evt/2019-06-precision-num/support11.pdf)
                        [support 2](attachments/evt/2019-06-precision-num/support12.mp4)
                        [support 3](attachments/evt/2019-06-precision-num/support13.mp4)
                        [support 4](attachments/evt/2019-06-precision-num/support14.mp4)
        
                    
                    
                
                .. event:: Clôture
                    :begin: 16:35
                    :end: 17:00
                    :speaker:  Organisateurs
        
                    
                    


.. section:: Organisation
    :class: orga

    - Anne Cadiou
    - Matthieu Haefele
