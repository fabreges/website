8èmes journées mésocentre
##########################

:date: 2015-09-08 20:38:12
:start_date: 2015-10-06
:end_date: 2015-10-06
:category: journee
:place: Paris, France


.. contents::

.. section:: Description
    :class: description

    Les 8èmes journées mésocentres auront lieu le 6/10/2015 à l'`Institut Henri Poincaré <http://www.ihp.fr>`__ à Paris.
    Au programme cette année, vous retrouverez une place importante consacrée aux  présentations effectuées par des mésocentres, en particulier autour des relations avec les PME.
    Au programme également, vous pourrez découvrir le nouveau catalogue national des formations en calcul scientifique.
    Note que le lendemain dans le même lieu se tiendra la 3ème édition des journées mésochallenges du projet Equip@Meso.
    Il y a actuellement 76 inscrits (5 Oct 2015). 


.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 06-10-2015

                .. event:: Ouverture
                    :begin: 09:00
                    :end: 09:20
                    :speaker: Groupe Calcul

                .. event:: Ministère
                    :begin: 09:20
                    :end: 09:50
                    :speaker: Laurent Crouzet
                    :support: attachments/spip/IMG/pdf/mesr.pdf

                .. event:: Genci
                    :begin: 09:50
                    :end: 10:20
                    :speaker: Jean-Philippe Proux
                    :support: attachments/spip/IMG/pdf/genci.pdf

                .. event:: CNRS
                    :begin: 10:20
                    :end: 10:50
                    :speaker: Michel Daydé
                    :support: attachments/spip/IMG/pdf/cocin.pdf

                .. event:: Catalogue des Formations
                    :begin: 11:20
                    :end: 11:50
                    :speaker: Caroline Bligny, IMAG, CNRS et Loïc Gouarin, LMO, CNRS
                    :support: attachments/spip/IMG/pdf/focal.pdf

                .. event:: Offre vers les PME sur le site de Grenoble
                    :begin: 11:50
                    :end: 12:20
                    :speaker: Stéphane Labbé 
                    :support: attachments/spip/IMG/pdf/hpc-2015.pdf

                .. event:: Restitution rapport mésos 2015
                    :begin: 12:20
                    :end: 12:30
                    :speaker: Romaric David
                    :support: attachments/spip/IMG/pdf/2015_pres_mesos.pdf

                .. event:: Mise en place de l'offre vers les PME au mésocentre de l'Université de Strasbourg
                    :begin: 14:00
                    :end: 14:30
                    :speaker: Anna Plessa et Romaric David
                    :support: attachments/spip/IMG/pdf/2015_pres_mesos.pdf

                .. event:: UPMC - Institut du Calcul Scientifique
                    :begin: 14:30
                    :end: 15:00
                    :speaker: Pascal Frey 
                    :support: attachments/spip/IMG/pdf/ics.pdf

                .. event:: Évolution des moyens de calcul à l'Université de Pau et des Pays de l'Adour
                    :begin: 15:00
                    :end: 15:30
                    :speaker: Jacques Hertzberg 
                    :support: attachments/spip/IMG/pdf/uppa.pdf

                .. event:: Retour d'expérience Mésocentre de l'Université de la Réunion
                    :begin: 16:00
                    :end: 16:20
                    :speaker: Delphine Ramalingom 
                    :support: attachments/spip/IMG/pdf/reunion.pdf

                .. event:: Mise en place du mésocentre de l'école centrale de Nantes
                    :begin: 16:20
                    :end: 16:40
                    :speaker: Richard Randriatoamanana 
                    :support: attachments/spip/IMG/pdf/ici.pdf

                .. event:: ANR
                    :begin: 16:40
                    :end: 17:00
                    :speaker: Mark Asch 
                    :support: attachments/spip/IMG/pdf/anr.pdf


.. section:: Organisation
    :class: orga

    - Romaric David (Direction Informatique, Strasbourg)
    - Violaine Louvet (GRICAD)
