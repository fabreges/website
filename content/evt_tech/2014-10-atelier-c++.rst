Atelier C++ : vers 2011 et au-delà
##################################

:date: 2014-07-03 11:53:09
:category: formation
:authors: Loïc Gouarin
:place: Paris
:start_date: 2014-10-29
:end_date: 2014-10-31


.. contents::

.. section:: Decription
  :class: description

  Le groupe Calcul vous propose un atelier "C++ : vers 2011 et au-delà "
  du 29 au 31 octobre 2014

  Cet atelier sera réalisé par `Joël Falcou <https://www.lri.fr/~falcou/>`_ du Laboratoire de Recherche en Informatique d'Orsay.

  `Supports de cours <attachments/spip/IMG/zip/cpp_and_beyond.zip>`_

  L'atelier se tiendra à l'`IHP <https://www.ihp.fr>`_ en salle 314.  Les horaires des journées sont

  - mercredi: 10h - 17h30
  - jeudi: 9h - 17h30
  - vendredi: 9h - 17h30

.. section:: Programme
  :class: description

  Le langage C++ a connu un regain d’intérêt depuis l'adoption du standard 2011
  et de 2014. Ces évolutions du langage ont pour objectif de rendre plus sûr,
  plus performant et plus simple le développement d'applications critiques et
  de logiciels d'infrastructures.

  Cette formation va parcourir les différentes nouveautés du langage en
  s'attachant à démontrer les applicabilités et leurs avantages dans des
  scénarios concrets. Les thèmes abordés sont:

    - Simplicité

      - Déduction de type automatique: auto, decltype, trailing return type
      - Boucles généralisées
      - Fonctions anonymes
      - Généricité appliquée: template alias, générique variadique

    - Sûreté

      - RAII et ses applications
      - Pointeur à sémantique forte et sémantique faible
      - Conteneurs et Algorithmes

    - Performances

      - Le multi-threading via std::thread
      - Parallélisation via Future
      - Polymorphisme Statique vs Polymorphisme Dynamique

    - Application au design applicatif
    - Application au design de bibliothèque
