Workshop "Masse de données : I/O, format de fichier, visualisation et archivage"
################################################################################

:date: 2010-06-28 07:56:32
:category: journee
:place: ENS, Lyon
:start_date: 2011-01-13
:end_date: 2011-01-13

.. contents::

.. section:: Description
  :class: description

  L'augmentation de la puissance des machines de calcul conduit à l'accroissement de la complexité des simulations numériques et de la taille des domaines géométriques considérés.
  Les besoins en données d'entrée et en sauvegarde et post-traitement des résultats sont donc de plus en plus importants.

  L'objectif de cette journée était d'échanger autour de la problématique des entrées/sorties des codes de calcul, de la visualisation, du stockage, de l'archivage et de la distribution des données.

  Cette journée a été co-organisée par le projet CIRA, le projet CHPID du cluster ISLE et le réseau Calcul.

.. section:: Programme
    :class: programme

        .. schedule::

            .. day:: 13-01-2011

                .. event:: Présentation de HDF4
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Bruno Six, Equipe ICARE, Lille
                    :support: attachments/spip/Documents/Manifestations/CIRA2011/Presentation_HDF4.pdf

                .. event:: L'archivage et le traitement des données de télédétection à ICARE : présentation des solutions technologiques et des services aux utilisateurs
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Jean-Marc Nicolas, Equipe ICARE, Lille
                    :support: attachments/spip/Documents/Manifestations/CIRA2011/archivage-icare.pdf

                .. event:: Retour d'expérience sur HDF
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Bruno Six, Equipe ICARE, Lille
                    :support: attachments/spip/Documents/Manifestations/CIRA2011/Retour_Experience_HDF.pdf

                .. event:: L'archivage et le traitement des données de télédétection à ICARE : présentation des solutions technologiques et des services aux utilisateurs
                    :begin: 09:00
                    :end: 10:00
                    :speaker: Jean-Marc Nicolas, Equipe ICARE, Lille
                    :support: attachments/spip/Documents/Manifestations/CIRA2011/archivage-icare.pdf

  - *La problématique du post-traitement, introduction à HDF5 et XDMF* , **Matthieu Haefele** , Max-Planck-Institut fuer Plasmaphysik, Muenchen.  `Support <attachments/spip/Documents/Manifestations/CIRA2011/2011-01_haefele_IO-workshop_Lyon.pdf>`__
  - *Les entrées/sorties pour le Calcul Hautes Performances* , **Matthieu Haefele** , Max-Planck-Institut fuer Plasmaphysik, Muenchen.  `Support <attachments/spip/Documents/Manifestations/CIRA2011/2011-01_haefele_parallel_IO-workshop_Lyon.pdf>`__
  - *Expériences d'entrées-sorties sur machines massivement parallèles* , **Philippe Wautelet** , IDRIS, Orsay.  `Support <attachments/spip/Documents/Manifestations/CIRA2011/IDRIS_io_lyon2011.pdf>`__
  - *S'affranchir du format de données avec la visualisation in-situ* , **Jean Favre** , CSCS, Lugano.  `Support <attachments/spip/Documents/Manifestations/CIRA2011/JFavre.pdf>`__
