7èmes journées mésocentres
##########################

:date: 2014-06-30 09:53:47
:category: journee
:authors: Loïc Gouarin
:start_date: 2014-10-07
:end_date: 2014-10-07
:place: Paris

.. contents::

.. section:: Description
  :class: description

  Le 7 octobre 2014, à l'`Institut Henri Poincaré <http://www.ihp.fr>`__, Paris

  Comme chaque année, nous aurons des présentations en lien avec le calcul
  haute performance des acteurs majeurs en France (Genci, le ministère
  ESR, CNRS/COCIN, ANR, Renater, ...).

  Nous aurons également des présentations sur la structuration du calcul
  intensif chez nos homologues européens.

  Petit panel des applications phares dans les mésocentres: `ici <attachments/spip/IMG/pdf/journee-mesocentre2014.pdf>`_


.. section:: Programme
  :class: programme

  .. schedule::

    .. day:: 07-10-2014

      .. event:: Genci et Prace
        :begin: 09:30
        :end: 10:00
        :speaker: Catherine Rivière
        :support: attachments/spip/IMG/pdf/Genci-meso2014.pdf
  
      .. event:: Intervention du Ministère de l’Enseignement Supérieur et de la Recherche
        :begin: 10:00
        :end: 10:30
        :speaker: Mark Asch
        :support: attachments/spip/IMG/pdf/MESR-meso2014.pdf
  
      .. event:: Bilan des activités du Cocin (CNRS)
        :begin: 10:30
        :end: 11:00
        :speaker: Michel Bidoit
        :support: attachments/spip/IMG/pdf/COCIN-meso2014.pdf
  
      .. event:: H2020
        :begin: 11:00
        :end: 11:30
        :speaker: Jean-Pierre Caminade
        :support: attachments/spip/IMG/pdf/H2020-meso2014.pdf
  
      .. event:: Structuration du Calcul Intensif en Allemagne
        :begin: 11:30
        :end: 12:00
        :speaker: Wolfgang Nagel
        :support: attachments/spip/IMG/pdf/Gauss-Allianz-meso2014.pdf
  
      .. event:: Structuration du Calcul Intensif en Angleterre
        :begin: 12:00
        :end: 12:30
        :speaker: Adrian Jackson (EPCC)
        :support: attachments/spip/IMG/pdf/EPCC-meso2014.pdf
  
      .. event:: Intervention de la CPU
        :begin: 14:00
        :end: 14:30
        :speaker: François Germinet
  
      .. event:: Intervention de l’ANR
        :begin: 14:30
        :end: 15:00
        :speaker: Jean-Yves Berthou
        :support: attachments/spip/IMG/pdf/ANR-meso2014.pdf
  
      .. event:: Intervention de Renater
        :begin: 15:00
        :end: 15:30
        :speaker: Nicolas Garnier
        :support: attachments/spip/IMG/pdf/renater-meso2014.pdf
  
      .. event:: Structuration du Calcul Intensif en Espagne
        :begin: 15:30
        :end: 16:00
        :speaker: Sergi Girona
        :support: attachments/spip/IMG/pdf/BSC-meso2014.pdf
  
      .. event:: Structuration du Calcul Intensif au Luxembourg
        :begin: 16:00
        :end: 16:30
        :speaker: Sébastien Varrette
  
      .. event:: Maison de la Simulation de Champagne-Ardenne
        :begin: 16:30
        :end: 17:00
        :speaker: Arnaud Renard
        :support: attachments/spip/IMG/pdf/romeo-meso2014.pdf
  
      .. event:: Rapport mésocentres
        :begin: 17:00
        :end: 17:30
        :speaker: Reseau calcul
        :support: attachments/spip/IMG/pdf/enquete-meso2014.pdf
