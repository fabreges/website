6ème Journées Mésocentres
##########################
:date: 2013-03-13 09:43:25
:category: journee
:authors: Romaric David
:start_date: 2013-09-13
:end_date: 2013-09-13
:place: Paris

.. contents::

.. section:: Description
  :class: description

  La journée aura lieu le 19 Septembre 2013, `Institut Henri Poincaré <http://www.ihp.fr>`_ à Paris. 
  Notez que le lendemain se tiendra dans le même lieu une `journée Equip@Meso dédiée à des "mésos-challenges" <http://genci.fr/fr/node/378>`_.

.. section:: Programme
  :class: programme

  .. schedule::

    .. day:: 13-09-2013

      .. event:: Intervention de Genci
        :begin: 09:30
        :end: 10:00
        :speaker: Catherine Rivière
        :support: attachments/spip/Documents/Journees/sept2013/PrésentationGENCIMésocentres.pdf
  
      .. event:: Intervention du Comité Stratégique du Calcul Intensif et du Ministère de l'Enseignement Supérieur et de la Rercherche
        :begin: 10:00
        :end: 10:30
        :speaker: Mark Asch
        :support: attachments/spip/IMG/pdf/Asch-CSCI-Meso.pdf
  
      .. event:: Intervention de l'ANR
        :begin: 11:15
        :end: 11:45
        :speaker: Jean-Yves Berthou
        :support: attachments/spip/IMG/pdf/GroupeCalcul19SeptJYB_-_v1-2.pdf
  
      .. event:: Modélisation du climat : défis, méthodes et exemples de résultats récents
        :begin: 11:45
        :end: 12:15
        :speaker: Marie-Alice Foujols, Institut Pierre Simon Laplace, CNRS
        :support: attachments/spip/Documents/Journees/sept2013/MESOCENTRE_MAFOUJOLS20130919.pdf
  
          `plaquette MissTerre <http://www.ipsl.fr/content/download/11581/105199/file/plaquette_MISSTERRE.pdf>`_
  
      .. event:: Bilan des activités du Cocin (CNRS)
        :begin: 12:15
        :end: 12:30
        :speaker: Michel Daydé
        :support: attachments/spip/IMG/pdf/COCIN-Presentation-Journee-meso-centres-2013.pdf
  
      .. event:: Intervention de France Grilles
        :begin: 14:00
        :end: 14:30
        :speaker: Geneviève Romier
        :support: attachments/spip/IMG/pdf/France-Grilles-mesocentres-19sept2013.pdf
  
      .. event:: L'histoire de la vie dans les génomes
        :begin: 14:30
        :end: 15:00
        :speaker: Bastien Boussau, Ancestrome Project, UMR 5558, Biométrie Biologie Evolutive, Université Lyon 1
        :support: attachments/spip/IMG/pdf/Mesocentres_Boussau.pdf
  
      .. event:: Intervention de Renater
        :begin: 15:00
        :end: 15:20
        :speaker: Simon Muyal
        :support: attachments/spip/IMG/pdf/RENATER-MESOCENTRES-20130919.pdf
  
      .. event:: DVFS pour le HPC : état et avancées de la collaboration UVSQ/Université de Strasbourg
        :begin: 15:20
        :end: 15:40
        :speaker: Benoit Pradelle, UVSQ
        :support: attachments/spip/IMG/pdf/MesoCentres.pdf
  
      .. event:: Intervention de la CPU
        :begin: 16:00
        :end: 16:20
        :speaker: Daniel Egret, Observatoire de Paris
  
      .. event:: Table ronde : Un tour d'horizon des nouveaux mésocentres 
        :begin: 16:20
        :end: 17:00
        :speaker: R. David
        :support: 
            [Slide Pascal Poulet](attachments/spip/IMG/pdf/present-13-06-13.pdf)
            [Coordination des mésocentres, rapport 2013](attachments/spip/IMG/pdf/enquete_mesos-2013-09-19.pdf)
  
        - Mésocentre de l'Université Antilles-Guyane, Pascal Poulet
        - Mésocentre de l'Université de Lille, Matthieu Marquillie


.. section:: Comité d'organisation
  :class: orga

    - Loïc Gouarin (Maths, Orsay)
    - Violaine Louvet (Institut Camille Jordan, Lyon)
    - Romaric David (Université de Strasbourg)



