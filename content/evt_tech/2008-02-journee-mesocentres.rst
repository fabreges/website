Journée Mésocentres |_| 2008
############################

:date: 2008-02-07 10:00:21
:start_date: 2008-02-13
:end_date: 2008-02-13
:category: journee
:tags: mesocentre
:place: Institut Henri Poincaré, Paris

.. contents::

.. section:: Description
    :class: description

    Le Groupe Calcul, en collaboration avec `ResInfo <https://resinfo.org>`__  a organisé une journée de rencontre autour du thème suivant : l'organisation et l'activité des mésocentres de calcul en France.
    Cette journée avait pour but de présenter le paysage des moyens de calcul français, notamment les pôles de calcul régionaux.
    Les exposés ont permis de présenter différents éclairages sur les intérêts multiples de ce type de moyens de calcul.

    Cette journée a été suivie le lendemain par `une réunion sur les grilles de calcul organisée par la CPU <2008-02-info-grilles.html>`__. Les comptes rendu de ces deux journées est `en ligne <attachments/spip/Documents/Journees/fev2008/CR-13_14-02.pdf>`__.


    **A NOTER :**  Cette journée a été webcastée. `Les vidéos sont en ligne <https://webcast.in2p3.fr/container/les_mesocentres>`__.

.. section:: Date et lieu
   :class: description

   Le 13 février 2008.

   `Institut Henri Poincaré <http://www.ihp.fr/>`__, amphi Hermite.


.. section:: Programme
   :class: programme

      .. schedule::

	 .. day:: 13-02-2008

            .. break_event:: Accueil
	       :begin: 09:30
               :end: 09:40

            .. event:: Introduction
	       :begin: 09:40
               :end: 10:15
               :speaker: Françoise Berthoud & Violaine Louvet
	       :support: attachments/spip/Documents/Journees/fev2008/pdf_Meso-centres-13-02.pdf

	       Mésocentres : Etat des lieux en France en février 2008.

	    .. event:: GENCI et PRACE
	       :begin: 10:15
               :end: 10:30
               :speaker: Alain Lichnewsky (GENCI)
	       :support: attachments/spip/Documents/Journees/fev2008/GENCI-IHP-JourneeMeso-Fevrier2008.pdf

	    .. event:: Le calcul haute performance en France
               :begin: 10:30
               :end: 10:45
               :speaker: Serge Petiton (ORAP)
	       :support: attachments/spip/Documents/Journees/fev2008/meso-ORAP-IHP-2.pdf

	    .. event:: L'institut des Grilles du CNRS
               :begin: 10:45
               :end: 11:15
               :speaker: Guy Wormser
	       :support: attachments/spip/Documents/Journees/fev2008/institut_des_grilles_meso.pdf

	    .. event:: L'initiative nationale pour les grilles de production : le point de vue de la CPU
               :begin: 11:15
               :end: 11:30
               :speaker: Laurent Desbat, Stéphane Cordier
	       :support: attachments/spip/Documents/Journees/fev2008/SCetLD_JMesoCentre130208.pdf

	    .. event:: Pôle Haut-normand de Modélisation Numérique
               :begin: 11:30
               :end: 12:00
               :speaker: Guy Moebs, Hervé Prigent
	       :support: attachments/spip/Documents/Journees/fev2008/2008-02-13_Pres-CRIHAN-Mesocentres.pdf

	       Le CRIHAN, association de loi 1901 créée à l'initiative du Conseil Régional, a pour mission d'aider les organismes publics et privés à développer leurs activités de recherche, d'enseignement, et de développement basées sur l'utilisation des nouvelles technologies de communication et sur l'informatique. Le CRIHAN est responsable de la mise en oeuvre du Pôle Régional de Modélisation Numérique qui regroupe des ressources matérielles et logicielles mutualisées, accessibles à la communauté académique et aux laboratoires de recherche privés au travers du réseau régional SYRHANO (internet non marchand). En 2007, des grappes de calcul IBM/AIX (1,1TFlops), Intel/Linux (0,3MFlops) et un ensemble de logiciels destiné à la modélisation moléculaire ont constitué l'essentiel des ressources mutualisées. Ces équipements évolueront en fonction des besoins au cours du Contrat de Projets 2007/2013.

	    .. event:: Expériences de dynamique moléculaire ab initio sur le mésocentre Phynum-CIMENT
               :begin: 12:00
               :end: 12:30
               :speaker: Alain Pasturel
	       :support: attachments/spip/Documents/Journees/fev2008/pdf_MESO-INFOI_Pasturel.pdf

	    .. break_event:: Buffet
	       :begin: 12:30
               :end: 14:00

            .. event:: ROMEO : le centre de calcul régional de Champagne-Ardenne
               :begin: 14:00
               :end: 14:30
               :speaker: Michael Krajecki
	       :support: attachments/spip/Documents/Journees/fev2008/mesoCentreROMEO.pdf

	    .. event:: Le CECPV : un mésocentre conçu pour et par ses utilisateurs
               :begin: 14:30
               :end: 15:00
               :speaker: Romaric David
	       :support: attachments/spip/Documents/Journees/fev2008/pdf_cecpv.pdf

	       Le CECPV est le mésocentre de l'Université Louis Pasteur de Strasbourg. Après dix ans d'existence, nous proposons un retour d'expérience sur le coeur de notre travail que représente le support technique et scientifique de proxmité. Au quotidien ce support permet de connaitre finement les besoins des utilisateurs et de mettre à disposition en conséquence les ressources de calcul. Sur le long terme, la confiance réciproque générée par ce travail permet d'envisager des opérations originales, comme la mutualisation des ressources de calcul parallèle. Nous nous demanderons si ce dernier projet, en plus de renforcer l'existence de la communauté "calcul parallèle", permet d'imaginer durablement de nouveaux modes de renouvellement des machines.

	    .. event:: CIMENT, un regroupement de pôles mésoinformatique mutualisés par une grille légère CIGRI
               :begin: 15:00
               :end: 15:30
               :speaker: Bruno Bzeznik, Laurent Desbat
	       :support: attachments/spip/Documents/Journees/fev2008/cigri-2008-02_journee_meso.pdf

	       CiGri est un projet libre de grille de calcul basé sur le principe du "best-effort". Cette grille permet d'exploiter les ressources libres des différentes machines d'un mesocentre pour les mettre à la disposition d'applications nécessitant l'éxécution d'un très grand nombre de tâches (applications multiparamétriques). Les applications pouvant être portées sur la grille soulagent localement les grappes et sont non intrusives pour les utilisateurs locaux des grappes qui composent la grille. CiGri est associé au projet de gestionnaire de ressources OAR également disponible sous licence GPL.

	    .. break_event:: Pause
	       :begin: 15:30
               :end: 15:50

            .. event:: Quelles ressources informatiques sont utilisées en modélisation du climat global à l'IPSL ?
               :begin: 15:50
               :end: 16:20
               :speaker: Marie-Alice Foujols
	       :support: attachments/spip/Documents/Journees/fev2008/Calcul_20080213_MAFoujols.pdf

	       La modélisation du climat global est un domaine classique d'utilisation des moyens de calcul intensif. L'exposé détaillera les ressources informatiques utilisées dans ce domaine, du poste de travail individuel aux centres de calcul nationaux et internationaux (Earth Simulator) sans oublier les mésocentres. Une analyse prospective des besoins sera également esquissée.

	    .. event:: Le mésocentre informatique à l'Université de Savoie : collaboration interdisciplinaire CNRS-IN2P3-UdS
               :begin: 16:20
               :end: 16:40
               :speaker: Nadine Neyroud, Roman Kossakowsky
	       :support: attachments/spip/Documents/Journees/fev2008/pdf_Colloque_Grille_13fev08_MUST.pdf

	       Le mésocentre informatique à l'Université de Savoie mutualise les moyens informatiques pour le calcul intensif, le stockage et l'accès à la grille européenne EGEE pour les laboratoires chambériens et annéciens. La convergence des besoins du laboratoire LAPP pour le traitement des futures données du LHC au CERN et la croissance des besoins en calcul des autres laboratoires ont permis de coordonner les actions de configuration, de financement, de mise en place, de gestion et de fonctionnement de ce centre.

	    .. event:: Conclusions et Discussions
               :begin: 16:40
               :end: 17:00
