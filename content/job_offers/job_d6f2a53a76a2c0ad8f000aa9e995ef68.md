Title: Radioastronomy imager accelerated on FPGA through High Level Synthesis
Date: 2021-07-08 11:26
Slug: job_d6f2a53a76a2c0ad8f000aa9e995ef68
Category: job
Authors: Nicolas Gac
Email: nicolas.gac@l2s.centralesupelec.fr
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: L2S, CentraleSupelec, Plateau de Saclay (91)
Job_Duration: 1 an (extension possible)
Job_Website: https://dark-era.pages.centralesupelec.fr
Job_Employer: CentralaSupelec
Expiration_Date: 2021-12-31
Attachment: job_d6f2a53a76a2c0ad8f000aa9e995ef68_attachment.pdf

**SKA computing, an HPC challenge.** The exascale radio telescope Square Kilometre Array (SKA) [1] will require supercomputers with high technical demands. The Science Data Processor (SDP) pipeline [2] in charge of producing the multidimensional images of the sky will have to execute in realtime a complex algorithm chain with data coming from telescopes at an incredible rate of several Tb/s and limited storage possibilities. The SDP will also have to be as green as possible with an energy budget of only 1 MWatt for 250 Petaflops.

**FPGA as an alternative to GPU.** Aside from the GPU mainstream architecture, alternative accelerators present better power-efficiency and cannot be already aside the road for the final SDP implementation. Among the alternative solutions, FPGA is an hardware architecture offering a unique fine-grain task and data parallelism compared to architectures based on processors like CPU, GPU or Kalray MPPA with a design dedicated to algorithms. However, the usual synthesis flows require hard- ware expertise and long implementation time. One of the promises of the emerging High Level Synthesis (HLS) tools is to make FPGA development accessible by software engineers with hardware implementa- tions generated from software programming languages like C, C++, or OpenCL. Afterwards, the FPGA design can be optimised gradually with the integration of hardware blocks. The dedicated FPGA solutions usually outperform GPU ones using the whole available computing power and avoiding memory congestion. First results obtained by the Astron Team for radioastronomy are already encouraging [3].

**A collaborative work.** This work will part of the ANR project, Dark-Era [4] which aims to : (i) build SimSDP, a rapid prototyping tool providing exascale simulations from dataflow algorithm descrip- tion, (ii) explore low power accelerators like FPGA or Kalray MPPA as alternatives to mainstream GPU architecture and (iii) be source of proposals for SKA computing and promoting french contribu- tions such as ddfacet [5]. Dark-Era gathers from the SimGrid [6] development Team at IRISA, the PREESM [7] development team at IETR, the inverse problem team at L2S, and two radio astron- omy teams at Observatories of Paris and Cˆote d’Azur. The inverse problems team (GPI) has a mixed expertise in architecture and signal processing with a long-term experience in deconvolution applied to astronomy ; On that topic, a PhD is in progress working on GPU acceleration in collaboration with Atos-Bull [8]. The GPI has driven research works on Intel FPGA acceleration through HLS tools since 2016 for tomography reconstruction, another inverse problem [9, 10].

**PostDoc Goal.** Exploration of the potential of FPGA acceleration with High Level Synthesis tools will be done through the design of an SDP prototype for NenuFAR [11] ; this very large low-frequency radio telescope located at Nan ̧cay Observatory has been inaugurated in Oct. 2019. It will produce visibility throughput about one hundred times lower than SKA1-LOW and adjustable to the single node prototype capabilities. The FPGA prototype will be designed through Intel FPGA SDK for OpenCL ; The FPGA prototype will be set up at Nanc ̧ay, connected to the correlator output visibility stream, to run in realtime its SDP pipeline. A main interest of this study is to deliver performance feedbacks in time, memory and energy to SimSDP. Indeed evaluation of the performance gain using FPGA inside HPC nodes in this specific use case, will be particulary useful to assess which role FPGA could play in future SKA like HPC projects.

**Candidate Profile.**

1. PhD in computer science (or signal processing);
2. Experience in computing acceleration on FPGA (Intel or Xilinx) or GPU (Cuda/OpenCL); 
3. Good background in signal processing;
4. Experience in publishing high quality research papers.
