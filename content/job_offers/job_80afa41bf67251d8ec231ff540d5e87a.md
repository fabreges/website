Title: Ingé spécialiste en traitement de données « Projet SWIO-Energy »
Date: 2020-03-20 11:05
Slug: job_80afa41bf67251d8ec231ff540d5e87a
Category: job
Authors: Patrick Jeanty
Email: patrick.jeanty@univ-reunion.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Saint-Denis-de-La-Réunion
Job_Duration: 24 mois
Job_Website: https://www.le2p.fr 
Job_Employer: EnergyLAB - Université de La Réunion
Expiration_Date: 2020-06-12
Attachment: job_80afa41bf67251d8ec231ff540d5e87a_attachment.pdf

L’université de La Réunion (UR) recrute un(e) ingénieur(e) en traitement de données dans le
cadre d’une opération de coopération régionale Océan Indien autour d’un projet scientifique
d’études climatiques dans la zone SOOI (Sud-Ouest de l’Océan Indien), le partenaire principal
étant l’Université de Maurice (UoM).
L’expertise du Laboratoire Energétique, Electronique et Procédés (LE2P - EnergyLAB), porteur du
projet, se situe à la fois dans le domaine de la métrologie au sol (l’équipe gère un réseau d’une
quinzaine de stations météorologiques sur le territoire réunionnais mais également dans les pays
de la zone), et dans le domaine de la modélisation régionale climatique (depuis 2011, l’équipe
met en oeuvre une approche dynamique de descente d’échelle basée sur l’utilisation de modèles
régionaux de climat dits RMCs forcés sur leurs bords par : 1) des modèles globaux de climat dits
GCMs, 2) des réanalyses climatiques).
Le projet SWIO-Energy, à financement européen INTERREG V 2014-2020, a ainsi pour objectif
principal d’étudier, pour la première fois dans la région, l’impact du changement climatique sur
les ressources renouvelables (solaire, éolien) à l’échelle locale à La Réunion et à Maurice en se
basant sur des mesures au sol, de l’imagerie satellitale et sur la simulation à l’aide de modèles
régionaux de climat.
L’ingénieur(e) intervient sous la responsabilité du chef de projet chapeauté par le responsable
scientifique du LE2P en charge du projet et en lien avec l’IGR et l’IGE Calculs Scientifiques du
laboratoire.

L’ingénieur(e) Traitement de Données rejoint le LE2P pour la première partie du projet (18 mois)
afin d’assurer :
**Données au sol**
Il(Elle) a pour missions d’assurer : 1) la collecte des données utiles pour le projet, 2) si applicable,
le contrôle de la qualité de celles-ci et 3) leur saisie en base.
En liaison avec l’IGR du LE2P et le technicien recruté par ailleurs sur le projet – et s’agissant du
point 2) précédent, il(elle) participe notamment aux opérations d’étalonnage des pyranomètres,
ainsi qu’aux aménagements de la plateforme BSRN (Baseline Surface Radiation Network) et du
centre d’étalonnage indoor. Il(Elle) est en fait le garant de la qualité des informations alimentant
la base de données radiométriques du laboratoire ainsi que la base internationale du BSRN.
En liaison avec les Principal Investigators (PIs) instrument des caméras imageurs de ciel et du radar BASTA au LACy, il(elle) est en charge également de l’exploitation des données is-sues de ces instruments.
**Imagerie satellitaire**
De la même façon, l’ingénieur(e) prend en charge le recueil des données satellitaires néces-saires au projet (qu’il s’agisse des produits radiatifs ou de nébulosité disponibles en ligne gratuitement ou des produits nuages développés à la station SEAS-OI tel que prévu dans le projet). Il(Elle) vérifie leur disponibilité, leur qualité ainsi que leur sauvegarde.
 **Analyse de données**
Tout au long du projet, son rôle est de préparer et de mettre en base les jeux de données observationnelles (mesures terrain, images satellitaires) utilisés par l’ensemble des parte-naires du projet. Ce travail se fera en liaison avec l’IGE Calculs Scientifiques du laboratoire ainsi que l’IGR Modélisation recruté par ailleurs sur le projet.
Plus spécifiquement, l’ingénieur(e) Traitement de données procède à la validation des don-nées satellitaires par les données au sol disponibles (en liaison directe avec les partenaires impliqués, notamment le LACy).
**Reporting**
Ses tâches comprennent la rédaction et l’édition régulières de rapports techniques à desti-nation des partenaires.

De formation scientifique supérieure minimale niveau Bac + 5, il(elle) aura avant tout des compétences dans les domaines suivants :

*	instrumentation, traitement du signal / images
*	imagerie satellitaire
*	analyses statistiques
*	traitement / gestion de données, base de données
*	communication RS232, TCP/IP… 
*	environnement Linux
*	outils informatiques de base : bureautique, Web
*	bonne connaissance de la langue anglaise

Les connaissances ci-après sont souhaitables :

*	énergies renouvelables liées aux gisements solaire et éolien
*	métrologie, instrumentation solaire / météorologique
*	techniques de traitement des Big Data
*	techniques et procédures d’étalonnage
*	milieu de la recherche
*	fonctionnement d’un établissement universitaire

Rigoureux et méthodique, l’ingénieur(e) Traitement de données aura les capacités de :

*	concevoir les solutions techniques nécessaires 
*	autonomie, adaptation aux situations
*	aisance relationnelle et rédactionnelle 

Une première expérience dans la mise en œuvre d’opérations de ce type constitue un atout.
