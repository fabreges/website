Title: Administrateur (rice) de cluster de calcul Linux
Date: 2019-11-26 09:47
Slug: job_e248ec5cee50b1bf791d85afbca6bbf6
Category: job
Authors: LEGRAIN Mallory
Email: recrutement@icm-institute.org
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: PARIS 
Job_Duration: 
Job_Website: 
Job_Employer: Institut du cerveau et de la moelle épinière
Expiration_Date: 2020-02-18
Attachment: job_e248ec5cee50b1bf791d85afbca6bbf6_attachment.pdf

Venez rejoindre un projet technologique au service de la recherche médicale au sein d’une équipe dynamique et passionnée. 
En rejoignant l’équipe de la DSI, vous aurez l’opportunité de mettre en place un projet d’extension d’un Cluster et de vous impliquer dans la stratégie de l’institut pour développer le calcul scientifique auprès des équipes de recherche en neuroscience et des partenaires institutionnels et privés.

MISSIONS PRINCIPALES

-	Mise en place d’un projet d’extension de cluster
-	Administrer le cluster de calcul en lien avec les architectes infrastructure,
-	Gérer les évolutions du cluster, proposer et mettre en place des solutions innovantes, multi-cloud
-	Gérer les environnements de production et de pré-production nécessaires,
-	Administrer les outils de monitoring (Intégration, maintenance...),
-	Superviser, analyser la performance et optimiser les systèmes sous gestion,
-	Assurer le support niveau 2 pour le cluster
-	Suivre et résoudre les incidents, installation de logiciels, packages, groupes nécessaires,
-	Participer à la mise en place des procédures d&#39;administration, à la structuration des outils de support et à la documentation utilisateur et technique,
-	Accompagner les scientifiques dans l’utilisation du cluster pour l’automatisation et la mise en parallèle des traitements,
-	Dispenser aux utilisateurs les formations sur le cluster et son écosystème.
SAVOIR
-	Titulaire d’un diplôme Bac + 4 informatique minimum,
-	Une expérience dans le monde de la santé et de la recherche est souhaitée.

SAVOIR-FAIRE

-	Vous possédez idéalement au moins 2 ans d’expérience dans un environnement similaire,
-	Vous avez une connaissance opérationnelle d’un cluster Linux et vous êtes à même de concevoir, installer et exploiter une infrastructure de ce type et de rédiger des procédures d’exploitation et des documentations utilisateur,
-	Vous maîtrisez les outils d’ordonnancement des tâches de calcul et les outils associés tels que Slurm, 
-	Vous avez déjà fait du support niveau 2 et utilisé des outils de ticketing (GLPI, etc.),
-	Vous avez la maîtrise des environnements Linux,
-	Vous avez une bonne connaissance de CUDA, MPI/OpenMPI
-	Vous avez de bonnes notions des environnements cluster et notamment des réseaux à faible latence,
-	Maîtrise de l&#39;anglais technique nécessaire.

SAVOIR-ETRE

-	Vous avez un sens du relationnel, du service aux utilisateurs et du travail en équipe,
-	Vous êtes autonome et suivez les évolutions techniques,
-	Vous êtes passionné(e) par l’environnement Linux et le calcul scientifique parallèle,
-	Vous appréciez transmettre votre savoir et former les utilisateurs.
