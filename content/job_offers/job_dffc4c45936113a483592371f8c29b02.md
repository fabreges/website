Title: Simulations numériques de l’impact de gouttes sur substrat solide
Date: 2021-05-28 08:14
Slug: job_dffc4c45936113a483592371f8c29b02
Category: job
Authors: Stéphane Glockner
Email: glockner@bordeaux-inp.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Bordeaux
Job_Duration: 3 ans
Job_Website: https://notus-cfd.org/
Job_Employer: I2M
Expiration_Date: 2021-08-20
Attachment: 

**Contexte**
La thèse s’inscrit dans le projet « Aventurine : vers un clone numérique de la projection plasma de suspensions à usage industriel ». Les techniques de revêtement de surface sont des technologies indispensables pour répondre aux enjeux de l’aéronautique comme pour la création de barrières thermiques ou pour les revêtements anti-usure de haute performance. Ces technologies doivent aussi répondre aux exigences de la réglementation REACH1 et tenir compte du problème d’approvisionnement en matériaux dits critiques. Ces exigences couplées avec le développement de nouveaux matériaux et des conditions d’utilisation de plus en plus extrêmes obligent à des progrès rapides et importants sur ces techniques de traitements de surface. Parmi les procédés envisageables, la projection plasma permet de fabriquer des revêtements céramiques. 
Les techniques de projection plasma sont classées pour l’aéronautique dans les procédés spéciaux dont les éléments de sortie ne peuvent être vérifiés que par une surveillance ou une mesure effectuée a posteriori et dont les déficiences n’apparaissent de ce fait, qu’une fois le produit en usage. La simulation numérique permet alors de grandement accélérer la compréhension des phénomènes pour assister la mise en œuvre expérimentale du procédé. Elle nécessite une connaissance profonde des phénomènes, et une analyse précise des comportements fluides et thermiques à l’échelle des gouttes pour accéder à des conditions opératoires optimales.
Le projet vise à développer un clone numérique d’un procédé de revêtement émergeant au niveau industriel, essentiellement dans la filière aéronautique : la projection plasma de suspension. Ce procédé autorise de nombreuses applications (barrières thermiques de nouvelle génération pour les turbines à gaz aéronautiques et industrielles, revêtements anti-usure à haute performance, revêtements glaciophobes sur ailes d’avion, revêtements antimicrobiens, etc.), et il a la potentialité de répondre à des enjeux technologiques d&#39;aujourd&#39;hui. 
Le projet est une collaboration entre le laboratoire IRCER de Limoges et l’I2M de Bordeaux. La thèse se déroulera au laboratoire I2M de Bordeaux.

**Objectifs de la thèse**
La représentation numérique de l’impact de suspensions est fondamentale pour étudier la structure du revêtement formé par pulvérisation. La thèse a comme objectif de modéliser et simuler l’impact de plusieurs particules, et d’estimer le régime d’impact et de transferts thermiques en fonction des conditions d’impact (vitesse, diamètre, propriétés des matériaux, topologie de surface de substrat…). Le code massivement parallèle de mécanique des fluides Notus (<https://notus-cfd.org>), développé à l’I2M (équipe Mécanique des Fluides Numérique du département TREFLE), sera utilisé pour la résolution des équations de Navier-Stokes, la représentation des interfaces libres et la prise en compte des transferts thermiques.
La thèse se focalisera sur le procédé de projection thermique. Elle est orientée sur le développement numérique de méthodes capables de restituer le comportement d’interfaces mobiles des gouttes lors de l’impact, sous des contraintes fortes (grandes vitesses, haute température, fortes déformations, changement de phase). Il s’agira de compléter le code Notus pour tenir compte des différents phénomènes responsables de l’écoulement complexe des gouttes, et de la solidification provoquée par les transferts thermiques entre le substrat et les particules fluides. Le thèse sera ainsi constituée de deux étapes majeures : la mise en place et/où l’adaptation et l’optimisation de modules complémentaires représentatifs des phénomènes physiques relatifs à l’écoulement de gouttes et des transferts thermiques (tension de surface, résistance thermique de contact, changement de phase)   et la simulation numérique massivement parallèle (sur des supercalculateurs régionaux et nationaux) de gouttes pour interpréter les relations entre conditions d’impact et morphologie des gouttes post-impact.

**Profil souhaité**
Le candidat devra faire preuve d’une solide formation en méthodes numériques et développement associés. Il aura en outre une implication forte en modélisation et simulation numérique, analyse physique du comportement fluide et thermique des matériaux. Le candidat devra ainsi faire preuve d’organisation, de prise d’initiatives, aura un goût prononcé pour les phénomènes d’écoulement et de transferts thermiques et leur résolution numérique. Une bonne maîtrise du FORTRAN (2003) est demandée, des compétences en parallélisme (MPI/OpenMP) seront appréciées.

**Candidature**
Faire parvenir aux contacts ci-dessous un CV et une lettre de motivation détaillant l’intérêt du/de la candidat(e) pour le sujet, les relevés de notes de Licence et Master (ou diplômes équivalents), les copies des rapports de stage et les coordonnées de personnes référentes.

**Contacts / encadrement**
Dr. Cédric Le Bot - I2M			Dr. Stéphane Glockner – I2M
<lebot@enscbp.fr>			          <glockner@bordeaux-inp.fr>

**Financement**
Bourse Région Nouvelle Aquitaine / Bordeaux INP
