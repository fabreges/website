Title: Ingénieur Calcul Scientifique (H/F) 
Date: 2021-04-12 12:04
Slug: job_80d02ee62e7e3bd97a3f84ff5b1fea4b
Category: job
Authors: Simon Lopez
Email: s.lopez@brgm.fr
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Orléans
Job_Duration: 
Job_Website: https://brgm-recrute.talent-soft.com/offre-de-emploi/emploi-ingenieur-e-en-calcul-scientifique-h-f_1741.aspx
Job_Employer: BRGM
Expiration_Date: 2021-07-05
Attachment: 

Vous serez intégré(e) à l’équipe Cartographie, Imagerie et Modélisation du sous-sol (CIM) de la direction des Géoressources (DGR). Dans le cadre de votre mission, vous participerez à travers différents projets au développement collaboratif de codes de simulation des transferts de masse et d’énergie dans les milieux géologiques complexes qui seront utilisées dans le cadre d’études couvrant l’ensemble des thématiques du BRGM (caractérisation du sous-sol, ressources minérales, ressources en eau, risques naturels, géothermie, après-mine…). L’un des objectifs du poste est de devenir à court terme l’un des développeurs principaux du code ComPASS. A plus long terme, votre positionnement transverse à toutes les thématiques de l’unité CIM vous permettra également d’apporter vos compétences en mathématiques appliquées et votre savoir-faire numérique en appui aux différents projets de l’équipe couvrant des champs thématiques comme la modélisation géologique, les problématiques de maillage, des techniques d’apprentissage/classification des données…

Dans ce cadre, vous serez principalement chargé(e) de :

- implémenter de nouvelles méthodes numériques pour la résolution des transferts de masse et d’énergie dans les milieux géologiques complexes (géométries et propriétés physiques contrastées, fortes non linéarités), et de nouveaux modèles physiques
- vous approprier le code ComPASS pour assurer son développement et sa maintenance (intégration continue) en veillant à ce qu’il réponde aux besoins opérationnels du BRGM et de ses partenaires et participer à l’animation de la communauté autour de ce code,
- développer des partenariats tant avec les utilisateurs qu’avec les inventeurs de ces méthodes,
- apporter ponctuellement votre expertise en développement pour adapter les codes que vous développerez aux besoins concrets d’études pluridisciplinaires réalisées par vos collègues,
- participer à un effort de structuration et de modularisation des outils numériques existants pour la modélisation des transferts en milieu poreux en interaction avec les autres directions opérationnelles du BRGM,
- contribuer à concevoir des projets en réponse à des appels d&#39;offres nationaux et internationaux, et à les réaliser,
- valoriser les résultats de votre travail au travers de rapports, communications dans des congrès internationaux et d’articles dans des revues scientifiques.
