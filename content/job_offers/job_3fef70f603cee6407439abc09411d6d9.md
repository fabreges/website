Title: Big Data et HPC Architect GPU H/F
Date: 2020-10-05 13:33
Slug: job_3fef70f603cee6407439abc09411d6d9
Category: job
Authors: Alexandre Baussard
Email: alexandre.baussard@utt.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Troyes
Job_Duration: 2 mois
Job_Website: https://www.utt.fr/
Job_Employer: Université de technologie de Troyes
Expiration_Date: 2020-12-31
Attachment: job_3fef70f603cee6407439abc09411d6d9_attachment.pdf

Dans le cadre des activités de ses équipes (plus particulièrement, l’équipe M2S), le département ROSAS de l’Université de Technologie de Troyes souhaite se dote d’une infrastructure de traitement Big Data pour héberger les expérimentations des chercheurs pour des projets de recherches et des contrats industriels. Cette infrastructure a pour vocation de mettre à disposition des chercheurs et de leur partenaires (i) d’une part un environnement de stockage distribué pour ce qui concernent les bases de données, (ii) et d’autre part une capacité de déploiement adaptée aux demandes des différents acteurs de la plateforme qui travaillent sur des projets variés.
Pour ce qui concerne le stockage distribué, il offrira la flexibilité et la scalabilité nécessaire à l’hébergement à la demande de bases de données multi-format et de « Vélocité » différentes. Concernant la capacité de déploiement deux solutions sont envisagées, la première consiste à « encapsuler » chaque expérimentation dans un container isolé et indépendant mais partageant les mêmes ressources. La deuxième est d’utiliser un environnement virtuel type via « conda ».

Il s’agit donc de fournir un environnement de développement et de pré-production pour des algorithmes d’analyse de données et de calcul haute performance (HPC) et les technologies associées.

L’ingénieur(e) recruté(e) sera en interaction avec les chercheuses et chercheurs et ingénieur(e)s de projets de l’équipe M2S au sein du département ROSAS. Elle/il sera aussi en étroite collaboration avec la Direction du Numérique (DNum) de l’UTT.