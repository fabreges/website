Title: Ingénrieur de recherche &#34;Base de données et protocoles de communication&#34;
Date: 2021-01-25 11:42
Slug: job_3178d1c828f6896fdd3a330eb4c0d382
Category: job
Authors: Damour Cédric
Email: cedric.damour@univ-reunion.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Saint-Denis de La Réunion
Job_Duration: 24 mois
Job_Website: https://www.le2p.fr/
Job_Employer: Université de La Réunion
Expiration_Date: 2021-02-20
Attachment: job_3178d1c828f6896fdd3a330eb4c0d382_attachment.pdf

Nous recrutons un ingénieur de recherche en Base de données et protocoles de communication dans le cadre du projet FEDER DETECT (deux ans).

Pour plus d&#39;information et pour postuler, voir le fichier joint.

Vous pouvez également nous contacter directement par mail si vous souhaitez plus d&#39;information.

Cédric DAMOUR
MCF HDR au laboratoire Energy-Lab (<https://www.le2p.fr/>) 
Porteur du projet DETECT
