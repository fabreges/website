Title: Ingénieur HPC/Cloud Computing
Date: 2021-04-02 12:31
Slug: job_4d9abdaccb502db0d929e74d942d6753
Category: job
Authors: Nicolas BENOIT
Email: nicolas.benoit@sorbonne-universite.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Paris
Job_Duration: 1 an (renouvelable)
Job_Website: https://sacado.sorbonne-universite.fr
Job_Employer: Sorbonne Université
Expiration_Date: 2021-06-25
Attachment: job_4d9abdaccb502db0d929e74d942d6753_attachment.pdf

**Contexte**
Au sein de Sorbonne Université, l&#39;unité de service SACADO (Service d’Aide au Calcul et d’Analyse de Données) met à disposition de la communauté recherche son expertise des évolutions technologiques et scientifiques du numérique (simulation, modélisation, analyse, apprentissage).
Elle gère la plateforme mutualisée de calcul et d’analyse de données, et propose un catalogue de services et d&#39;activités de conseil, d&#39;assistance et d&#39;ingénierie.

**Rôle**
En tant qu’ingénieur HPC/Cloud de l’unité de service, vous assurerez deux missions principales :

- l’accompagnement des utilisateurs de la plateforme mutualisée de calcul et d’analyse de données, notamment sur les aspects cloud et analyse de données (HPDA) ;
- la participation à des projets menés en lien avec des équipes de recherche, en particulier à la prise de poste, un projet d’intégration d’un système d’analyse rapide de génome (UF-WGA) en lien avec la faculté de médecine et l’équipe ayant développé la suite d’outils HiLive (<https://gitlab.com/lokat/HiLive2>).

Dans le cadre de ces missions, les activités liées à ce poste incluent notamment :

- le développement de scripts ou d’outils pour la gestion et l’exploitation de la plateforme ;
- la documentation de nouveaux outils et de procédures dans la base de connaissances ;
- la veille technologique et le prototypage pour faire évoluer la plateforme ;
- la prise en charge de demandes d’assistance transmises par les utilisateurs de la plateforme.

**Profil**
Vous êtes diplômé d’une école d’ingénieur ou d’une université (Master, Doctorat) et pouvez justifier d’un intérêt, voire d’une expérience, pour le calcul haute-performance (HPC) ou le cloud computing et l’analyse de données (HPDA).
Vous êtes à l’aise avec les systèmes basés sur Linux, les langages Python et Shell.
Vous disposez d’une bonne culture générale informatique (système d’exploitation, architecture, réseau, programmation, containers, virtualisation, etc.) et vous connaissez les particularités et les outils du calcul parallèle et du cloud : performance, passage à l’échelle, déploiement, réseau rapide, stockage distribué, … 
En plus de la langue française, vous maîtrisez également l’anglais technique à l’oral et à l’écrit.
Vous êtes curieux, persévérant et avez le sens du service.

**Conditions particulières d’exercice**

- Type de contrat : CDD 1 an (renouvelable et évolution possible vers un CDI)
- Quotité de travail : 100 %
- Rémunération : selon profil
- Lieu de travail : campus Pierre et Marie Curie de Sorbonne Université (Paris, Jussieu)
		
**Personne à contacter**
Nicolas BENOIT – Directeur de l’unité de service SACADO
