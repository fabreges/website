Title: Data Scientist
Date: 2019-12-04 10:59
Slug: job_62cbfc24ecd5cd45188d551f11bda4ae
Category: job
Authors: ALLIOD Charlotte
Email: charlotte.alliod@altran.com
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: LYON, Vaise
Job_Duration: 
Job_Website: 
Job_Employer: Altran
Expiration_Date: 2020-02-26
Attachment: job_62cbfc24ecd5cd45188d551f11bda4ae_attachment.pdf

**Contexte :**
Les modèles de maintenance existants (la maintenance curative qui consiste à réparer une fois la panne survenue, ou la maintenance systématique qui consiste à planifier des interventions de maintenance d’après une périodicité d’usage) ont montré leurs limites : immobilisation de l’appareil et plan de maintenance peu efficace. La maintenance prédictive va plus loin et permet de suivre en temps réel l’état de santé d’un équipement, de prédire les pannes en relevant des données provenant de plusieurs sources (capteurs par exemple) et de mettre en place des modèles de prédiction de pannes grâce à des analyses statistiques et des corrélations entre les données.

Dans ce cadre, la Direction R&amp;I France, entité de recherche interne du groupe Altran, mène le projet MODINS (Models, Data Industry 4.0). Ce projet vise à développer des méthodologies basées sur l’exploitation des données industrielles dans le but de faire de la maintenance prédictive et d’optimiser les opérations liées à la maintenance.

**Missions :**
En collaboration avec le chef de projet MODINS, vous aurez pour missions :
De mener les travaux de recherche sur la partie analyse de données en suivant les axes pré-établis :

- Intégrer et pré-traiter des données.
- Proposer, tester et mettre en place des méthodes de Data Mining / Machine Learning pour diagnostiquer et prévoir les pannes à partir de données capteurs.
- Proposer, tester et mettre en place des méthodes de Text Mining (NLP) / Process Mining pour traiter des tickets d’incidents ou logs d’événements afin optimiser la maintenance.
- Rendre une vue synthétique de vos résultats.
- D’assurer les objectifs de production scientifique, tels que veille, publication d’articles scientifiques, conférences et communications internes. 

**Profil :**

- Docteur en mathématiques appliquées, statistiques, informatique, physique, traitement du signal, bio-informatique ou équivalent.
- Compétences en Data Mining, Machine Learning (avoir déjà travaillé sur des données réelles serait un plus).
- Des compétences en Text Mining, Process Mining, Traitement du signal, Architecture de base de données seraient un plus.
- Maîtrise d’un langage de programmation d’analyse de données : Python (Pandas, Numpy, Scikit-learn…), R…
- Autonome, travail en équipe, ouvert d’esprit, force de proposition.
- Maîtrise du français et de l’anglais, à l’écrit comme à l’oral.
