Title: Ingénieur, Differentiation Algorithmique pour CUDA
Date: 2021-05-21 12:31
Slug: job_7dfbc5ece50ea8b68db9b1af957c7332
Category: job
Authors: Laurent HASCOET
Email: laurent.hascoet@inria.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Sophia-Antipolis
Job_Duration: 2 ans
Job_Website: https://jobs.inria.fr/public/classic/fr/offres/2021-03734
Job_Employer: INRIA
Expiration_Date: 2021-07-18
Attachment: 

L&#39;objectif est d&#39;intégrer dans la prochaine plateforme de calcul scientifique (CFD) de l&#39;ONERA, SoNICE, des calculs de gradient par méthodes adjointes via la Différentiation Algorithmique (DA). La différentiation devra prendre en compte l&#39;aspect calcul parallèle de SoNICE, basé en partie sur CUDA.

Les capacités de DA seront fournies par l&#39;outil Tapenade de l&#39;INRIA. L&#39;objectif est d&#39;étendre Tapenade pour différentier des codes CUDA, et plus particulièrement les procédures de SoNICE utilisant CUDA. On considèrera à la fois les modes Tangent et Adjoint de la DA.