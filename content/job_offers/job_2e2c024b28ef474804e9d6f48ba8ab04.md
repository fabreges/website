Title: Etude et mise en place d’un couplage neutronique-dynamique rapide pour l’étude du régime surcritique prompt dans les réacteurs à sels fondus de type RNR.
Date: 2020-10-27 10:29
Slug: job_2e2c024b28ef474804e9d6f48ba8ab04
Category: job
Authors: Drui Florence
Email: florence.drui@cea.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: CEA Saclay
Job_Duration: 6 mois
Job_Website: 
Job_Employer: CEA/DES
Expiration_Date: 2021-01-15
Attachment: 

**Contexte :**
Les réacteurs à sels fondus (MSR pour Molten Salt Reactor en anglais) figurent parmi les 6 concepts de réacteurs de 4ème génération. Les MSR utilisent un combustible liquide qui leur confère des propriétés ouvrant la voie vers des innovations de rupture majeures par rapport au réacteurs « classiques » (utilisant du combustible solide) en matière de sûreté et d’acceptation des populations, de pilotage avec les ENR, de gestion des déchets en alternative à l’enfouissement profond, de compacité permettant des réacteurs de 100 MWe à 1500 MWe sans profondes modifications, etc. De nombreux acteurs internationaux étudient actuellement ces réacteurs, pour des applications différentes, comme par exemple le nucléaire spatial.

Le CEA renoue avec les MSR (de type RNR) depuis 2017 et développe des outils de calcul couplés entre la neutronique et la thermohydraulique. Ces outils de calcul visent à modéliser les MSR dans le plus grand nombre de situations possibles. Parmi ces situations, la situation accidentelle de passage en régime sur-critique prompt est un aspect incontournable à traiter pour la sûreté, pour lequel le CEA ne possède pas encore d’outil de modélisation pour réaliser des études. La physique de cette situation ne permet plus d’utiliser, avec le code de neutronique, un code de thermohydraulique usuel (basé sur des écoulements incompressibles) car elle requiert de faire appel à un code de dynamique rapide qui traite des interactions entre les fluides et les structures. La propagation des ondes est alors fortement couplée avec la production neutronique que ce soit dans le sel liquide ou lors la formation de vapeurs à haute température.
EUROPLEXUS est le code de référence du CEA pour le calcul de phénomènes transitoires rapides (impact, choc, explosion…) et d’interactions fluide-structure. Il permet la résolution de calculs de propagation des ondes de détente rapide de bulles de gaz sous pression et leur influence sur les structures. 

**Objectif du stage et déroulement :**
L’objectif du stage est la réalisation d’un outil couplé entre le code de neutronique APOLLO3 et le code EUROPLEXUS, et d’étudier le comportement d’un MSR à spectre rapide en situation de sur-criticité prompte : compréhension du phénomène, interactions entre les aspects mécaniques et neutroniques, tenue des structures, etc… L’outil de couplage ICOCO, déjà utilisé entre autres pour faire interagir APOLLO3 avec le code de thermohydraulique TrioCFD, sera utilisé.

Après une prise en main des différents outils, le stage se déroulera en plusieurs étapes :

- A l’aide d’EUROPLEXUS, étudier la propagation d’ondes de pression résultant de l’introduction d’une source d’énergie localisée au sein d’une géométrie simple.
- Déterminer une modélisation pertinente d’un sel fondu dans le cadre d’un écoulement compressible.
- Identifier les grandeurs d’intérêt nécessaires au couplage avec les phénomènes neutroniques, mettre en place les structures informatiques d’interaction entre les codes via ICOCO.
- Construire progressivement l’outil de modélisation du phénomène sur-critique prompt. En l’absence d’un tel outil mécaniste, un outil thermohydraulique paramétrable développé par un Doctorant du DER/SESI sera utilisé pour fournir des données d’entrée à EUROPLEXUS et conforter les résultats du nouveau schéma de calcul développé lors du stage.  

La problématique est particulièrement vaste et ce stage constitue la première étape notamment pour la compréhension des phénomènes, la mise en place des outils et les pistes de travail à venir. Le stagiaire sera vivement invité à participer aux réflexions afin de participer à la construction de son travail et pourra être amené à proposer des évolutions et/ou intervenir dans le développement des outils.

**Lieu et encadrement :**
Le stagiaire sera basé au CEA Saclay, au sein du DM2S/SEMT (équipe EUROPLEXUS) et sera encadré par une équipe également composée de personnes du DM2S/SERMA (pour le code APOLLO3 et la physique générale des MSR) et un doctorant (DER/SESI) travaillant sur les insertions de réactivité des MSR.

Il est demandé au candidat d’avoir un goût prononcé pour :

- Les sujets innovants et en rupture avec l’existant,
- Les équipes pluridisciplinaires et composées de plusieurs interlocuteurs,
- La communication et la prise d’initiative,
- La programmation informatique (C++, Fortran, Python) pour coder les outils,
- Les problématiques physiques amont.

La connaissance des MSR et des enjeux qu’ils représentent sera un plus. 

**Contacts :**

- nicolas.lelong@cea.fr   01 69 08 40 18
- guillaume.campioni@cea.fr    06 74 06 91 31