Title: Ingénieur-e de recherche CNRS 
Date: 2021-06-03 11:14
Slug: job_3ee3f992bac5e5fdd8cf7efe572906d8
Category: job
Authors: Claire Lévy
Email: Claire.Levy@locean.ipsl.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Paris
Job_Duration: 
Job_Website: https://www.nemo-ocean.eu/
Job_Employer: CNRS
Expiration_Date: 2021-06-30
Attachment: 

 **NEMO est une plateforme numérique de modélisation de l&#39;océan. Parmi la dizaine de modèles de ce type dans le monde, c&#39;est le seul utilisé et développé à la fois par et pour la recherche académique, les services opérationnels et les modèles &#34;Système Terre&#34; utilisés pour les projections climatiques du GIEC.**

Le CNRS ouvre au recrutement un poste d&#39;ingénieur de recherche pour contribuer au développement de NEMO. Il s&#39;agit d&#39;un concours externe pour recruter un-e ingénieur-e CNRS (poste permanent, fonction publique). Les candidat-e-s interne (mobilité) sont bien sûr bienvenu-e-s aussi, d&#39;autant plus que ce poste n&#39;a pas été ouvert en mobilité interne auparavant. 
    
Le profil est précisé ici  <https://profilsdemplois.cnrs.fr/ords/afipprd/PG_PROFIL_PUBLIC_REFERENS.pAfficheProfilPublic?destination=CE2021&hidIdConcours=26277>
    
Pour plus d&#39;information, contacter Claire Lévy (<Claire.Levy@locean.ipsl.fr>).
Informations pour candidater: <http://www.dgdr.cnrs.fr/drhita/concoursita/>
Date limite de dépôt de candidatures: 30 juin 2021 à 13h (heure de Paris).
 
Pour les équivalences de diplômes étrangers c&#39;est ici:  <https://www.france-education-international.fr/en/enic-naric-page/recognition-of-foreign-qualifications-documents-issued>
