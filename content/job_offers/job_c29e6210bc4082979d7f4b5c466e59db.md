Title: Méthode d’homogénéisation sans maillage: Développement d’un code de calcul par FFT ; Passage à une implémentation GPU
Date: 2019-11-25 11:04
Slug: job_c29e6210bc4082979d7f4b5c466e59db
Category: job
Authors: Mikael Gueguen
Email: mikael.gueguen@ensma.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Poitiers Futuroscope
Job_Duration: 5 - 6 mois
Job_Website: https://www.pprime.fr/?q=fr/recherche-scientifique/d1/endommagement-et-durabilite
Job_Employer: ENSMA
Expiration_Date: 2020-02-17
Attachment: job_c29e6210bc4082979d7f4b5c466e59db_attachment.pdf

Une partie des activités de l’équipe « endommagement et durabilité » de l’institut pprime est relative à l’étude de l’influence de la microstructure sur le comportement de divers matériaux, et cela à différentes échelles. Tout matériau hétérogène peut également être assimilé à un matériau homogène à une certaine échelle. C’est sur cette observation que se base les méthodes d’homogénéisation.  Plusieurs approches d’homogénéisation existent dans la littérature. Les approches en champs moyens, qui sont des méthodes analytiques mais qui fournissent peu d’informations locales ; les méthodes en champs complets décrivant la microstructure et permettant dans ce cas de déterminer des champs de contraintes et de déformation dans les phases du matériau. Dans le cas de l’utilisation d’une discrétisation de l’espace 3D par les éléments finis, cette méthode nécessite un gros travail de mise en œuvre (géométrie, maillage, ...) et possède l’inconvénient d’être rapidement couteuse en temps et mémoire. Une autre approche en champs complets, reposant sur la Transformée de Fourier Rapide (FFT : Fast Fourier Transform) existe, et s’appuie directement sur les données des phases et des textures décrites dans l’image 3D. Cette image est généralement issue d’outils de caractérisation expérimentale (synchrotron, tomographe).
L’objectif du travail vise à poursuivre le développement d’un code de calcul d’homogénéisation développé dans l’équipe et basé sur la méthode FFT. Actuellement un parallélisme de type mémoire partagée est implémentée et codée en langage c++ via les instructions de l’api OpenMP (www.openmp.org). L’usage des cartes graphiques peut maintenant être réalisée aussi par des directives issues de cette norme depuis sa version 4.5, ce qui rend l&#39;utilisation des GPU plus facilement abordable. Le travail consistera donc à porter le code pour pouvoir utiliser les cartes GPU avec les directives OpenMP, ainsi que l’utilisation d’une librairie FFT utilisant les cartes GPU, en lieu et place de la librairie fftw (www.fftw.org) actuellement utilisée par le code. 
Une fois les développements réalisés, des tests et simulations seront réalisés en modélisant le comportement en plasticité cristalline d’un matériau métallique soumis à une charge imposée. 

