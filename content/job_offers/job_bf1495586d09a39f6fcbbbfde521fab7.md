Title: Chef-fe de projet ou expert-e en ingénierie des systèmes d&#39;information 
Date: 2021-06-03 09:16
Slug: job_bf1495586d09a39f6fcbbbfde521fab7
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Orsay (91)
Job_Duration: 
Job_Website: http://www.idris.fr/annonces/idris-recrute.html
Job_Employer: CNRS-IDRIS
Expiration_Date: 2021-06-30
Attachment: 

Dans le cadre des concours de recrutement externes CNRS 2021, l&#39;IDRIS recrute :

**Chef-fe de projet ou expert-e en ingénierie des systèmes d&#39;information (IR - concours n° 45)**

Au sein de l&#39;équipe système de l&#39;IDRIS, le centre national du CNRS pour le calcul numérique intensif de très haute performance (HPC) et l&#39;intelligence artificielle (IA), la personne recrutée participera au déploiement, à la mise en œuvre et à l&#39;administration de services associés à l&#39;architecture matérielle et logicielle des plates-formes (supercalculateurs et autres serveurs) du centre de calcul, et en premier lieu au supercalculateur Jean Zay qui a été ouvert à la production pour l&#39;ensemble des utilisateurs en octobre 2019. 


Pour consulter la fiche complète du concours : <https://profilsdemplois.cnrs.fr/ords/afipprd/PG_PROFIL_PUBLIC_REFERENS.pAfficheProfilPublic?destination=CE2021&hidIdConcours=26281>

Pour plus d&#39;information et/ou pour postuler, consulter le site web du CNRS : <http://www.dgdr.cnrs.fr/drhita/concoursita/>

**Date limite : mercredi 30 juin 2021 à 13h** 
