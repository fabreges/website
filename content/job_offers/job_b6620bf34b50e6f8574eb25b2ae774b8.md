Title: Analyse et restitution par simulation numérique des résultats d&#39;une campagne d&#39;expériences d&#39;accélération de protons par laser
Date: 2019-12-06 09:07
Slug: job_b6620bf34b50e6f8574eb25b2ae774b8
Category: job
Authors: BARDON
Email: matthieu.bardon@cea.fr
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: CEA-CESTA, 15 avenue des Sablières, 33114 Le Barp, France
Job_Duration: 5-6 mois
Job_Website: 
Job_Employer: CEA
Expiration_Date: 2020-02-28
Attachment: job_b6620bf34b50e6f8574eb25b2ae774b8_attachment.pdf

L&#39;objectif de ce stage est de participer à l’analyse des résultats de l’expérience PACMAN 2, réalisée sur l’installation laser LULI2000 au mois de Février 2020. Une première partie du stage sera dédiée au traitement des résultats expérimentaux à l’aide d’outils informatiques (programmes Matlab et Python). La seconde partie sera consacrée à la restitution de ces résultats par la simulation numérique à l’aide d&#39;un code 3D Particle In Cell.