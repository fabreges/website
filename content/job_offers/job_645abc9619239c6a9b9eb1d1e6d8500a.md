Title: Ingénieur chercheur en Intelligence Artificielle
Date: 2021-03-30 09:36
Slug: job_645abc9619239c6a9b9eb1d1e6d8500a
Category: job
Authors: Erwan ADAM
Email: erwan.adam@cea.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Saclay
Job_Duration: 1 an renouvelable jusque 3 ans
Job_Website: 
Job_Employer: CEA Saclay
Expiration_Date: 2021-12-31
Attachment: 

Entreprise : CEA Saclay ; STMF/LGLS
Mission : CDD de un an renouvelable jusqu&#39;à 3
Profil recherché : Docteur en IA, apprentissage machine

De l&#39;apport de l&#39;apprentissage machine sur la quantification d&#39;incertitudes ...

La simulation numérique est déployée de façon massive par les ingénieurs et chercheurs du CEA pour concevoir, étudier ou optimiser des systèmes physiques complexes. Lorsqu’elle est utilisée à des fins industrielles, la simulation numérique repose sur des modélisations approximatives et/ou simplifiées afin de respecter des contraintes de temps de calcul acceptables. Les incertitudes induites par ces approximations peuvent alors être représentées par des distributions de probabilité qui quantifient le degré de précision des quantités d’intérêt calculées par les simulateurs.

Au sein du laboratoire LGLS (Laboratoire de Génie Logiciel pour la Simulation) du CEA, vous évoluez dans une équipe spécialisée dans la modélisation et le traitement des incertitudes en simulation numérique et en intelligence artificielle. Vous contribuez au renforcement de cette équipe par l’étude théorique et l’implémentation des méthodes d’apprentissage statistique non supervisé.

Plus précisément, votre mission consiste à étudier et à comparer certaines de ces méthodes dont les GANs (Generative Adversarial Networks), les NFs (Normalizing Flows) et les techniques de MC (Monte Carlo) Dropout, pour lesquelles des résultats prometteurs ont été rapportés récemment pour estimer des densités de probabilité en grande dimension [1,2,3]. Le potentiel de déploiement de ces méthodes issues du champ de l’intelligence artificielle et du « machine learning » pour résoudre des problèmes de quantification d’incertitude devra être évalué à l’aune de cet état de l’art. Vous étudiez notamment leurs applications possibles pour traiter des problèmes de calage de paramètres dans les simulateurs et pour évaluer la confiance des métamodèles par réseaux de neurones. Les résultats et la pertinence de ces méthodes d’apprentissage non supervisé seront comparés à des méthodes formelles de quantification d’incertitude.

[1] N. Kovachki, R. Baptista, B. Hosseini, Y. Marzouk, Conditional Sampling With Monoton GANs, pre-publication arxiv, 2020.
[2] I. Kobyzev, S. J. D. Prince, M.A. Brubaker, Normalizing Flow : An Introduction and Review of Current Methods, IEEE transactions on pattern analysis and machine intelligence, 2020.
[3] Y. Gal, Z. Ghahramami, Dropout as a Bayesian Approximation: Representation Model Uncertainty in Deep Learning, arXiv:1506.02142v6

Profil recherché : Docteur en IA, apprentissage machine
Spécialités : Mathématiques, probabilités/statistiques, intelligence artificielle
Compétences techniques : Développement logiciel, Python, C++

Contact : Erwan Adam (<erwan.adam@cea.fr>), chef du LGLS
