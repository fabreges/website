Title: Ingénieur Expert(e) en calcul Scientifique (H/F)
Date: 2021-06-01 12:27
Slug: job_2a50414c608647b5ce1cdf507fa1aa70
Category: job
Authors: Mikael Gueguen
Email: mikael.gueguen@ensma.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Futuroscope - Poitiers
Job_Duration: 12 mois (renouvelable)
Job_Website: https://bit.ly/34t2qyN
Job_Employer: CNRS - Institut Pprime
Expiration_Date: 2021-07-24
Attachment: 

**Missions**
Au CNRS, sur le site du Futuroscope, l&#39;Institut Pprime recrute un(e) ingénieur(e) en calcul scientifique devra apporter son expertise pour l&#39;implémentation d&#39;éléments logiciels dans le code de calcul éléments finis de l&#39;équipe de recherche. Il(elle) soutiendra les développements numériques menés en appuyant les efforts d&#39;optimisation des codes de calcul pour permettre de traiter des problèmes de plus grandes tailles. Il(elle) participera à la gestion de projet des codes développés, et au développement des bonnes pratiques pour pérenniser et maintenir ces codes et leur documentation. Il(elle) participera aussi à rendre ces outils plus robustes et flexibles dans l&#39;optique d&#39;une distribution publique plus large.

**Activités**
L&#39;ingénieur(e) en calcul scientifique réalisera les activités suivantes :

- Programmation / mise en œuvre dans le solveur numérique éléments finis de l&#39;équipe de l&#39;ensemble des éléments logiciels permettant l&#39;usage de la librairie PETSc à travers la notion de maillage non structurés, et la prise en compte du caractère hétérogène des modèles/matériaux et des champs issus de la segmentation (vecteurs d&#39;orientation dans les composites, orientation cristallographique dans les polycristaux),
- Dans un premier temps, l&#39;implémentation se basera sur une loi de comportement élastique linéaire, le travail se poursuivra ensuite pour la prise en compte de comportement matériaux non linéaires,
- Réalisation, avec la collaboration des chercheurs, et mise en place et le suivi des calculs sur des cas d&#39;études bien sélectionnés en termes de microstructures, type et taille de problème,
- Suivi de versions, participation à la rédaction de la documentation des nouveaux développements et le transfert de compétences vers les chercheurs et doctorants de l&#39;équipe.

**Compétences**
L&#39;ingénieur(e) en calcul scientifique devra posséder les compétences suivantes :

- Base de compétences en calcul scientifique ; Modélisation et simulation numérique,
- Connaissances en mécanique numérique (niveau de compréhension des enjeux scientifiques derrière les développements),
- Connaissance des outils de gestion de source,
- Connaissance de la bibliothèque de calcul parallèle PETSc appréciée,
- Connaissance du logiciel Paraview appréciée,
- Connaissance de l&#39;outil de développement cmake appréciée,

**Savoir-faire requis**

- Calculs HPC,
- Conduite de projets de développement,
- Mise en œuvre des langages de programmation C++/C, Python, et des techniques de parallélisation (protocoles MPI, OpenMP).

**Savoir-être requis**

- Capacité à travailler en équipe,
- Capacités de communication, y compris en anglais.

**Diplômes** 
Jeune docteur(e) ou jeune diplômé(e) d&#39;école d&#39;ingénieur dans un ou plusieurs des domaines suivants :

- calcul numérique/scientifique
- mécanique et modélisation numérique
- mathématiques appliqués.

**Contexte de travail**
L&#39;équipe « Endommagement et Durabilité » de l&#39;Institut Pprime travaille sur la tenue mécanique de plusieurs familles de matériaux, en collaboration fréquente avec des entreprises des secteurs du transport (aéronautique et automobile) et de l&#39;énergie. Les études cherchent à mettre en relation la microstructure initiale du matériau, son évolution au cours du chargement, les mécanismes de déformation et d&#39;endommagement qui se créent et la réponse mécanique macroscopique.
L&#39;équipe développe pour cela depuis plusieurs années des outils numériques permettant le traitement d&#39;images tomographiques, le transfert des microstructures segmentées vers des codes éléments finis, leur maillage et enfin la simulation de leur réponse mécanique.

L&#39;effort porté sur ces développements est en lien avec les spécificités des microstructures étudiées : composites tissés 3D avec de très fines zones de matrice entre les renforts, polycristaux métalliques, composites à fibres courtes en forte densité, structure de défauts (porosités, fissures) de très faible taille et de morphologie complexe.
