Title: Geospatial image processing engineer
Date: 2020-03-02 16:07
Slug: job_ac2f985f2d3201b3e8a1add4e6a2104a
Category: job
Authors: Remi Cresson
Email: remi.cresson@inrae.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Montpellier, France
Job_Duration: 30 months
Job_Website: https://en.ird.fr/
Job_Employer: IRD
Expiration_Date: 2020-05-25
Attachment: job_ac2f985f2d3201b3e8a1add4e6a2104a_attachment.pdf

The UMR ESPACE-DEV lab is looking for a geospatial image processing engineer for a 30 months contract, located in Montpellier, France.

See attached PDF file for details.