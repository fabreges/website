Title: Closed-Loop Flow Control by Plasma Discharge and Machine Learning
Date: 2020-03-20 09:45
Slug: job_bf291de0d14ce799e517a0b126c1dfb0
Category: job
Authors: Laurent CORDIER
Email: Laurent.Cordier@univ-poitiers.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Institut Pprime (Poitiers)
Job_Duration: 3 ans
Job_Website: https://pprime.fr/
Job_Employer: CNRS
Expiration_Date: 2020-04-30
Attachment: job_bf291de0d14ce799e517a0b126c1dfb0_attachment.pdf

**Department:** Fluids, Thermal and Combustion Sciences
**Research teams:** Incompressible Turbulence and Control (ITC) / Electro-Fluido-Dynamics (EFD)
**PhD advisor:** Laurent CORDIER (DR CNRS – Pprime/ITC, Poitiers)
**Co-PhD advisor:** Philippe TRAORE (MCF HDR Université de Poitiers – Pprime/EFD, Poitiers)
**Contact for information:** Laurent.Cordier@univ-poitiers.fr 
**3-year contract:** 1715 € raw monthly salary. 50% funding guaranteed.

**Key-words:** flow control, closed-loop, plasma actuators, Machine Learning, Neural Networks, Reinforcement Learning

**Framework.**
In recent years, continuous progress has been made on the performance of both civilian and military aircraft and helicopters, particularly in terms of flight envelope, radiated noise, maneuverability, vibration, etc. However, further improvements can be achieved by using closed-loop fluid flow control around the machine. Compared to mechanical blowing or suction actuators more often used in flow control, the advantages of plasma actuators come from their non-intrusive nature, low-energy cost, and particularly short-reaction times. These actuators are generally composed of a system of electrodes installed on one of the walls of the area to be controlled. By applying a sufficient potential difference between these electrodes, a plasma discharge is generated, inducing an ion wind which creates a flow tangential to the wall in order to accelerate the flow, and especially modify the velocity profile in the boundary layer.

**Objectives.**
The efficiency of plasma actuators depends to a large extent on their positions on the wall, as well as on numerous other control hyper-parameters (number of electrodes, distances between them, potential difference or electrical power, shape of the electrical signal, frequency of the discharge, etc.). The objective of the thesis is to determine these parameters in order to optimize a previously established performance function. For this, a numerical optimization tool coupling simulation of complex electrostatic phenomena and closed-loop control will be developed. The work will be organized in two broadly coupled axes: i) development of efficient control strategies by machine learning, ii) improvement of the understanding and physical modeling of the mechanisms at work.

**Work program, methodologies and means.**
We propose to numerically derive closed-loop control strategies of different flows. We will treat the numerical simulation aspects of physical mechanisms (electrodynamics and fluid mechanics) and the development of innovative control strategies (Data Driven approaches based on machine learning methods).
We will study two rather emblematic types of flows:

- The flow behind an obstacle (cylinder, wing profile, backward facing step). This type of strongly separated flow is particularly interesting in cases where the objective of the control is to increase aircraft stealth.
- The mixing layer developing at the interaction of two coaxial jets (see Figure). In this application, the objective is to increase the mixing efficiency between the two jets by exciting the mixing layer with plasma discharges.

In the first part of this thesis, we will simulate the plasma discharge by solving the transport equations of the different electrons and densities of ionic species present in the gas. We will implement in Oracle 3D, the code developed in the EFD team, a 3-species model taking into account the reality of the physical mechanisms. In the second part, we will develop closed-loop control strategies based on machine learning methods, mainly neural networks and reinforcement learning.

This subject is supported by a half scholarship awarded by the “Direction Générale de l’Armement”. Additional funding will be requested within the framework of the Labex Interactifs (Pprime). This topic is at the heart of the CNRS Research Group &#34;Flow Control Separations&#34;, whose Director is Laurent Cordier (Pprime). 

**Applicant profile, prerequisites.**
Master in Fluid Mechanics / Applied Mathematics / Machine Learning. Appetite for interdisciplinary approaches and machine learning. Desire to go beyond the borders.
