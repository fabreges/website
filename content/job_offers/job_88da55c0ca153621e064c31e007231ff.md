Title: Administrateur-trice de bases de données / Intégrateur d&#39;applications 
Date: 2021-06-03 09:47
Slug: job_88da55c0ca153621e064c31e007231ff
Category: job
Authors: Geneviève Morvan
Email: communication@idris.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: Orsay (91)
Job_Duration: 
Job_Website: http://www.idris.fr/annonces/idris-recrute.html
Job_Employer: CNRS-IDRIS
Expiration_Date: 2021-06-30
Attachment: 

Dans le cadre des concours de recrutement externes CNRS 2021, l&#39;IDRIS recrute :

**Administrateur-trice de bases de données / Intégrateur d&#39;applications ( IE - concours n°101)**

Au sein de l&#39;équipe système de l&#39;IDRIS, l&#39;ingénieur-e d&#39;étude participera au déploiement, à la mise en œuvre et à l&#39;administration de services associés à l&#39;architecture matérielle et logicielle des plates-formes de stockage, traitement et mise à disposition de données du projet FITS (CNRS Federated IT services for Research Infrastructures) à destination des Infrastructures de Recherche/Très Grandes Infrastructures de Recherche représentant des communautés scientifiques de grande envergure.

Pour consulter la fiche complète du concours : <https://profilsdemplois.cnrs.fr/ords/afipprd/PG_PROFIL_PUBLIC_REFERENS.pAfficheProfilPublic?destination=CE2021&hidIdConcours=26201>

 Pour plus d&#39;information et/ou pour postuler, consulter le site web du CNRS : <http://www.dgdr.cnrs.fr/drhita/concoursita/>
 
Date limite : mercredi 30 juin 2021 à 13h 

