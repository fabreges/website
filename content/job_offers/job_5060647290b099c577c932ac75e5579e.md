Title: Ingénieur stagiaire HPC - Création d’outil pour le profiling de performance sur cluster
Date: 2020-10-21 12:55
Slug: job_5060647290b099c577c932ac75e5579e
Category: job
Authors: Sonia Portier
Email: stage@nextflow-software.com
Job_Type: Stage
Tags: stage
Template: job_offer
Job_Location: Nantes, France
Job_Duration: 6 mois
Job_Website: https://www.nextflow-software.com/
Job_Employer: Nextflow Software
Expiration_Date: 2021-01-13
Attachment: job_5060647290b099c577c932ac75e5579e_attachment.pdf

Start-up indépendante et éditeur de logiciels basée à Nantes, Nextflow Software développe et commercialise des logiciels d’ingénierie assistée par ordinateur (CAO) dans le domaine de la dynamique des fluides (CFD).
Nextflow s’adresse aux sociétés d’ingénierie et industriels développant et fabriquant des produits et des systèmes impliquant des écoulements de fluides, potentiellement avec des géométries complexes et des interactions avec des solides, dans le secteur de l’automobile, l’aéronautique, le maritime, etc. Grâce à son équipe hautement qualifiée composée d’ingénieurs et de docteurs et à un partenariat académique de plus de 10 ans avec les laboratoires de l’Ecole Centrale de Nantes (ECN), Nextflow Software ouvre de nouvelles perspectives dans le domaine de la simulation hydrodynamique.

**Contexte Technologique**
Nous développons des logiciels scientifiques cross-platform qui mettent en oeuvre des interfaces graphiques, du rendu 2D/3D, et des codes de calcul hautes performances (HPC) exécutés sur des clusters scientifiques.
Notre environnement de développement intègre des outils performants et innovants, faisant appel à l’intégration continue et la virtualisation.

**Poste et Missions**
Au sein de l’équipe R&amp;D, votre rôle consistera à la création d’outils pour le profiling de performance de nos nouveaux solveurs sur des clusters. Allant de l’automatisation de benchmark sur la CI a l’élaboration d’outil pour la visualisation des données de profiling provenant de supercalculateur, vous évoluerez dans un milieu innovant à forte contrainte de performance sur base de langage moderne (C++17).

Dans ce contexte, vous devrez :

- Améliorer au besoin les outils de profiling existant (C++)
- Concevoir et développer des nouveaux outils de profiling
- Automatiser les benchmarks existant sur la CI
- Concevoir et développer des outils pour le post traitement et la visualisation des données de profiling
- En fonction du temps et de votre appétence pour ce domaine : Identifier des axes d’amélioration de performance et développer les optimisations adéquate

**Votre Profil**
Vous êtes élève en école d’ingénieur ou licence d’informatique.
Passionné·e par le développement informatique, vous souhaitez vous forger une expérience dans la recherche et le développement au sein d’une équipe dynamique.
Les connaissances idéalement souhaitées pour ce stage sont :

- C++ (idéalement avec des notions de C++11)
- Notion de metaprogramming (templates &amp; traits)
- Notion en tests unitaires (GoogleTest)
- Notion en visualisation de donnée (Spreadsheet, Gnuplot, etc…)

Les connaissances optionnelles appréciées sont :

- Git, CMake
- Environnement : Windows / Linux
- Notion en MPI
- Notion en outils de profilage de performance (Intel VTune, Intel Advisor ou Valgrind)
- Notion en GitLab-CI
- Notion en Scrypting (Python, bash)
- Notion en Docker
- Bon niveau d’anglais

