Title: Software developer Ab-initio simulation
Date: 2020-02-04 15:41
Slug: job_4a429e119e42e43cf4cd04920e4308dc
Category: job
Authors: Paolo Giannozzi
Email: paolo.giannozzi@uniud.it
Job_Type: Post-doctorat
Tags: postdoc
Template: job_offer
Job_Location: Trieste, Italy
Job_Duration: 1+1 years
Job_Website: http://www.max-centre.eu/general-public
Job_Employer: IOM-CNR
Expiration_Date: 2020-03-02
Attachment: 

A post-doctoral position in the field of electronic-structure software development is available at IOM-CNR, Trieste, Italy. The position, funded by the MaX - Materials at the exascale EU Horizon2020 Centre of Excellence, is for one year, renewable for another year. Requirements include a PhD in a scientific discipline and research experience in computer simulations and/or in scientific software programming. Experience with density-functional theory (DFT) calculations, especially with the plane wave-pseudopotential method, is a plus.

The successful candidate will work on the Quantum ESPRESSO software distribution in one or more of the following topics, depending upon his/her skill set:
- development of new methods and algorithms for advanced DFT functionals;
- refactoring and modularisation of the code basis, notably for first-principle molecular dynamics, linear response, computational spectroscopy;
- porting of Quantum ESPRESSO  new heterogeneous accelerated architectures (e.g.,  GPUs);
- improvement of Quantum ESPRESSO user experience, users’ and developers’ documentation, testing.
The work requires coordination and interactions with the Quantum ESPRESSO users&#39;
and developers&#39; community and with the MaX EU Centre of Excellence.

The official announcement and forms are available at the following link:
  https://bandi.urp.cnr.it/doc-assegni/documentazione/9530_DOC_IT.pdf
(English version at page 10 and 17-21). Interested people, including those not satisfying all requirements, may contact Paolo Giannozzi &lt;paolo.giannozzi@uniud.it&gt; for more information.

