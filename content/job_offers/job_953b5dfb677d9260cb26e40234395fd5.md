Title: High Performance Computing (HPC) Infrastructure and architecture engineer (M/F)
Date: 2020-01-13 12:44
Slug: job_953b5dfb677d9260cb26e40234395fd5
Category: job
Authors: Sebastien Varrette
Email: Sebastien.Varrette@uni.lu
Job_Type: CDI
Tags: cdi
Template: job_offer
Job_Location: Luxembourg, Belval campus
Job_Duration: 
Job_Website: https://hpc.uni.lu
Job_Employer: University of Luxembourg
Expiration_Date: 2020-02-15
Attachment: 

The University of Luxembourg (UL) is seeking to hire a highly motivated and an outstanding High Performance Computing (HPC) Infrastructure and architecture engineer within the HPC team of the University of Luxembourg led by Prof. Pascal Bouvry (Head) and Dr. Sébastien Varrette (Deputy Head).

**High Performance Computing (HPC) Infrastructure and architecture engineer (M/F)**

* Ref : F1-50013978
* Permanent contract, 40 hours/week, competitive salary
* Starting date: as soon as possible
* Details and application: <http://emea3.mrted.ly/2dokl>

*Organization:*
Since 2006, the University of Luxembourg has invested into its own High Performance Computing (HPC) facilities. Special focus was laid on the development of large computing power combined with huge data storage capacity to accelerate the research performed in intensive computing and large-scale data analytic (Big Data). This characteristic distinguishes the HPC center at the university from many other HPC facilities, which often concentrate on only one of these two pillars. Nowadays, the UL HPC facility remains the biggest platform in Luxembourg and the HPC team is deeply involved in the national and European HPC developments. Further information can be found on [hpc.uni.lu](hpc.uni.lu).

*Your Role*
The HPC Infrastructure and architecture engineer will be part of the HPC team to contribute to the operational services of the UL facility of the University (especially at the networking and storage level) and to the research and knowledge in HPC by analyzing user needs, and tailoring solutions matching those needs.

Duties of the position include, but are not limited to:

* Contribution to the support of the HPC facilities and associated research infrastructures, for both the first line level (Computer Help Information Point) and second line support (HPC platform maintenance, R&amp;D, SLA enforcement) to troubleshoot and debug problems in our production systems
* Assistance to the HPC direction for the plan and design of the future infrastructure (hardware and software), to constantly meet the needs in a consistent, flexible and scalable way
* Contribution to the development of best-practices and cutting-edge/robust technologies in the HPC and devops ecosystem of the University; In particular, (s)he is expected to play a leading role in the management of the HPC network (both at the Ethernet and InfiniBand level) as well as the HPC storage (SpectrumScale/GPFS and Lustre)
* Ensuring the work quality and meeting deadlines
* Serving as a privileged interface with the users of the UL HPC platform and contributing to the tutoring and training of UL staff members

The HPC Infrastructure and architecture engineer will report to the HPC direction (head and deputy head). (S)he will also act as a research and development engineer on specific research projects.

**More details and application: see <http://emea3.mrted.ly/2dokl>**
