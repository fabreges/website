Title: Ingénieur Informatique Scientifique et Technique
Date: 2020-06-25 13:00
Slug: job_a3bdcf2df10f439f5dbda97e03937e67
Category: job
Authors: Majid OUNSY
Email: majid.ounsy@synchrotron-soleil.fr
Job_Type: CDD
Tags: cdd
Template: job_offer
Job_Location: Gif-sur-Yvette
Job_Duration: jusqu&#39;au 31/08/2022
Job_Website: https://www.synchrotron-soleil.fr/
Job_Employer: Synchrotron SOLEIL
Expiration_Date: 2020-10-31
Attachment: 

Ce poste est ouvert dans le cadre du projet européen ExPaNDS (<https://expands.eu/>). 

Il s&#39;agit principalement d&#39;assurer la mise en œuvre technique de solutions de type cloud adoptées par le projet ExPaNDS pour la fourniture d’une plateforme d’accès distants à des services logiciels scientifiques existants pour les utilisateurs des infrastructures nationales Synchrotron et Neutrons.

L&#39;activité inclut :

* Participation à la conception, au développement et aux tests d’une plateforme logicielle basée sur une architecture en microservices de type cloud, accessible via un portail web et permettant l’accès distant à des catalogues de données scientifiques ainsi qu’à des bureaux virtuels présentant aux utilisateurs de la plateforme des codes de calcul pré-packagés pour le traitement de ces données (Notebooks, applications Docker containerisées).
* Développement des adaptations logicielles nécessaires au déploiement de cette plateforme sur les infrastructures de calcul et de stockage locales à SOLEIL et sa connexion à des plateformes communautaires distantes (European Open Science Cloud).
* Collaboration avec les autres partenaires du projet, contribution aux réunions d’avancement du projet ou réunions spécifiques à des tâches du projet, présentation des travaux à l&#39;occasion de  séminaires internes ou externes.

Pour plus de détails voir <https://www.synchrotron-soleil.fr/fr/emplois/ingenieur-informatique-scientifique-et-technique>
