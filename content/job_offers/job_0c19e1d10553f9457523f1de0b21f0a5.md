Title: Outils de prédiction de la rupture basés sur le couplage mesure de champs, calculs EFs et machine-learning
Date: 2021-05-03 08:29
Slug: job_0c19e1d10553f9457523f1de0b21f0a5
Category: job
Authors: CDD
Email: pierre.kerfriden@mines-paristech.fr
Job_Type: Thèse
Tags: these
Template: job_offer
Job_Location: Centre des Matériaux, Mines ParisTech, Evry
Job_Duration: 36 mois
Job_Website: https://computationalengin.blogspot.com/
Job_Employer: Mines ParisTech
Expiration_Date: 2021-10-26
Attachment: 

[https://www.mat.minesparis.psl.eu/Recrutements/Theses/Details/-CONTRAT-DOCTORAL-Outils-de-prA-copy-diction-de-la-rupture-basA-copy-s-sur-le-couplage-mesure-de-champs-calculs-EFs-et-machine-learning/36638](https://www.mat.minesparis.psl.eu/Recrutements/Theses/Details/-CONTRAT-DOCTORAL-Outils-de-prA-copy-diction-de-la-rupture-basA-copy-s-sur-le-couplage-mesure-de-champs-calculs-EFs-et-machine-learning/36638)