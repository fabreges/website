Journée "Julia et Optimisation"
###############################

:date: 2019-04-16 09:10:03
:modified: 2019-04-16 09:10:03
:category: journee
:tags: julia, optimisation
:start_date: 2019-06-17
:end_date: 2019-06-17
:place: Nantes
:summary: Journée autour du langage Julia et de ses applications en optimisation.
:inscription_link: https://julialang.univ-nantes.fr/inscription-formulaire

.. contents::

.. section:: Description
    :class: description

    Cette journée soutenue par le groupe calcul a lieu le lundi 17 juin 2019 à l'Université de Nantes.

    Vous trouverez sur ce site l'ensemble des informations et le programme de la journée : https://julialang.univ-nantes.fr/journee-julia-et-optimisation/
