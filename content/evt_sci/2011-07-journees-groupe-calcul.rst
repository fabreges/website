Journées du Groupe Calcul
#########################

:date: 2011-02-18 12:18:52
:modified: 2011-02-18 12:18:52
:category: journee
:tags: gdr, réseau, calcul
:place: Paris
:start_date: 2011-07-05
:end_date: 2011-07-06
:summary: Le Groupe Calcul organise ses troisièmes journées !

.. contents::

.. section:: Description
    :class: description

    Le GDR Calcul a organisé ses journées annuelles les 5 et 6 juillet 2011 à l'`IHP <http://www.ihp.fr/>`_ sur le thème des problématiques de discrétisation spatiale.

    Le format de ces journées a été un peu novateur par rapport aux sessions précédentes puisqu'il a pris la forme :

    - de tutoriaux sur les sujets Galerkin Discontinu et Volumes finis
    - d'exposés de chercheurs confirmés
    - d'exposés de jeunes chercheurs.


.. section:: Support des présentations
    :class: description

    - `Méthodes volumes finis pour les problèmes elliptiques <2011-07-journees-groupe-calcul.html#methodes-volumes-finis-pour-les-problemes-elliptiques>`_, **F. Hubert**, Laboratoire d'Analyse, Probabilités et Topologie, Marseille. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/FHubert.pdf>`__

    - `Schéma à capture de choc et multirésolution adaptative pour la simulation d'écoulements visqueux compressibles <2011-07-journees-groupe-calcul.html#schema-a-capture-de-choc-et-multiresolution-adaptative-pour-la-simulation-decoulements-visqueux-compressibles>`_, **C. Tenaud**, LIMSI, Orsay. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/CTenaud.pdf>`__

    - `Un schéma de type volumes finis pour un modèle de croissance tissulaire <2011-07-journees-groupe-calcul.html#un-schema-de-type-volumes-finis-pour-un-modele-de-croissance-tissulaire>`_, **A. Uzureau**, doctorant Laboratoire de Mathématiques de Nantes. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/AUzureau.pdf>`__

    - `Influence de la géométrie du maillage sur le schéma de Godunov appliqué à l'équation des ondes dans le régime Bas Mach <2011-07-journees-groupe-calcul.html#influence-de-la-geometrie-du-maillage-sur-le-schema-de-godunov-applique-a-lequation-des-ondes-dans-le-regime-bas-mach>`_, **P. Omnes**, CEA Saclay, service mécanique des fluides, modélisation et études et Université Paris 13, LAGA. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/POmnes.pdf>`__

    - `Méthodes de raffinement hp pour l'équation de transport des neutrons <2011-07-journees-groupe-calcul.html#methodes-de-raffinement-hp-pour-lequation-de-transport-des-neutrons>`_, **D. Fournier**, doctorant CEA Cadarache. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/DFournier.pdf>`__

    - `Schémas volumes finis pour le problème de Stokes à viscosité variable <2011-07-journees-groupe-calcul.html#schemas-volumes-finis-pour-le-probleme-de-stokes-a-viscosite-variable>`_, **S. Krell**, INRIA Lille. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/SKrell.pdf>`__

    - `Couplage des écoulements souterrains avec le ruissellement - Méthodologie numérique et applications <2011-07-journees-groupe-calcul.html#couplage-des-ecoulements-souterrains-avec-le-ruissellement-methodologie-numerique-et-applications>`_, **P. Sochala**, BRGM Orléans. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/PSochala.pdf>`__

    - `Développement de méthodes DDFV pour les équations d'Euler <2011-07-journees-groupe-calcul.html#developpement-de-methodes-ddfv-pour-les-equations-deuler>`_, **V. Desveaux**, doctorant Laboratoire de Mathématiques de Nantes. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/VDesveaux.pdf>`__

    - `Tutoriel Galerkin Discontinu <2011-07-journees-groupe-calcul.html#tutoriel-galerkin-discontinu>`_, **D. Le Roux**, Institut Camille Jordan, Lyon. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/DLeRoux.pdf>`__

    - `Méthodes d'élements finis discontinus d'ordre élevé pour l'océan <2011-07-journees-groupe-calcul.html#methodes-delements-finis-discontinus-dordre-eleve-pour-locean>`_, **V. Legat**, Ecole Polytechnique de Louvain. `Support de la présentation <http://perso.uclouvain.be/vincent.legat/talks/2011-Paris-GDR.pdf>`__

    - `Etude d'une méthode Galerkin discontinue en maillages hybrides pour la résolution numérique des équations de Maxwell instationnaires <2011-07-journees-groupe-calcul.html#etude-dune-methode-galerkin-discontinue-en-maillages-hybrides-pour-la-resolution-numerique-des-equations-de-maxwell-instationnaires>`_. **C. Durochat**, doctorant INRIA Sophia Antipolis. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/CDurochat.pdf>`__

    - `Une nouvelle méthode cut-cell pour la simulation numérique d'écoulements bidimensionnels autour d'obstacles <2011-07-journees-groupe-calcul.html#une-nouvelle-methode-cut-cell-pour-la-simulation-numerique-decoulements-bidimensionnels-autour-dobstacles>`_, **N. James**, post-doctorant EPFL. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/NJames.pdf>`__

    - `Méthodes numériques d'homogénéisation pour des problèmes elliptiques non-linéaires non-monotones <2011-07-journees-groupe-calcul.html#methodes-numeriques-dhomogeneisation-pour-des-problemes-elliptiques-non-lineaires-non-monotones>`_, **G. Vilmart**, Ecole Polytechnique Fédérale de Lausanne. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/GVilmart.pdf>`__

    - `Méthode de Galerkin discontinues pour les écoulements multiphasiques compressibles <2011-07-journees-groupe-calcul.html#methode-de-galerkin-discontinues-pour-les-ecoulements-multiphasiques-compressibles>`_, **V. Perrier**, INRIA Bordeaux Sud Ouest. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/VPerrier.pdf>`__, `film 1 <attachments/spip/Documents/Journees/juil2011/bubble_vanderwaals.avi>`__, `film 2 <attachments/spip/Documents/Journees/juil2011/vortex_dg0_euler.avi>`__

    - `Estimateurs d'erreur a posteriori pour les équations de la magnétodynamique en formulation potentielle (A/φ) harmonique <2011-07-journees-groupe-calcul.html#estimateurs-derreur-a-posteriori-pour-les-equations-de-la-magnetodynamique-en-formulation-potentielle-aph-harmonique>`_, **Z. Tang**, doctorant Laboratoire Paul Painlevé, Lille. `Support de la présentation <attachments/spip/Documents/Journees/juil2011/ZTang.pdf>`__


.. section:: Résumés des exposés
    :class: description

    .. section:: Méthodes volumes finis pour les problèmes elliptiques.
        :class: description

        \F. Hubert, Laboratoire d'Analyse, Probabilités et Topologie, Marseille.

        Les méthodes volumes finis sont utilisées depuis de nombreuses années dans l'ingéniérie pour discrétiser des phénomènes physiques issus de la mécanique des fluides. L'intérêt de ces schémas est d'assurer une bonne conservation des propriétes physiques, comme la préservation des flux, les bornes des solutions,... Les études théoriques de ces méthodes sont, quant à elles, relativement récentes. Ce n'est qu'à la fin des années 80 qu'ont été proposés les premiers résultats de convergence des méthodes volumes finis pour des équations aux dérivées partielles de type laplacien ou lois de conservation hyperboliques. Jusqu'au début des années 2000, les équations elliptiques linéaires et/ou non linéaires de type Leray-Lions étaient essentiellement discrétisées par des techniques éléments finis utilisant la formulation variationnelle sous-jacente.

        Nous commencerons dans cet exposé par rappeler le principe des méthodes volumes finis pour le Laplacien sur des maillages dits "admissibles". Nous verons ensuite que les difficultés rencontrées pour la discrétisation par des méthodes volumes finis de telles équations sont semblables aux difficultés que l'on rencontre lorsque l'on veut utiliser des maillages très déformés pour le laplacien et nous décrirons une des méthodes (appelées DDFV "Discrete Duality Finite Volume") développées pour surmonter ces difficultés.

        - Bibliographie :

            [1] B. Andreianov, F. Boyer, and F. Hubert. {Discrete duality finite volume schemes for Leray-Lions type elliptic problems on general 2D-meshes}. Numerical method in PDE, 23(1):145–195, 2007.

            [2] F. Boyer and F. Hubert. *Finite volume method for 2d linear and nonlinear elliptic problems with discontinuities* . SIAM J. Numer. Anal., to appear, 2008.

            [3] K. Domelevo and P. Omnes, *A finite volume method for the Laplace equation on almost arbitrary two-dimensional grids* , M2AN Math. Model. Numer. Anal. 39 (2005), no. 6, 1203–1249.

            [4] Coudière, Y., Hubert, F.: *A 3D discrete duality finite volume method for nonlinear elliptic equation*  (2010) HAL, URL: http://hal.archives-ouvertes.fr/hal-00456837/fr.

            [5] R. Eymard, T. Gallouët, and R. Herbin, *Finite volume methods* , Handbook of numerical analysis, Vol. VII, Handb. Numer. Anal., VII, North-Holland, Amsterdam, 2000, pp. 713–1020.


    .. section:: Schéma à capture de choc et multirésolution adaptative pour la simulation d'écoulements visqueux compressibles.
        :class: description

        \C. Tenaud, LIMSI, Orsay.

        Dans le régime des hautes vitesses, les écoulements aérodynamiques présentent, pour la plupart, des interactions entre ondes de choc et couches cisaillées, phénomènes fortement instationnaires, qui restent difficiles à appréhender numériquement du fait de la multiplicité des échelles spatiales à représenter. En particulier, certains phénomènes (interaction onde de choc vorticité, production de vorticité par effet barocline, ...) ne peuvent être pris en compte par des modélisations (de type LES, par exemple) et l'utilisation de schémas de haute résolution couplée à un maillage localement très fin est indispensable à une prédiction fiable de tels écoulements. Outre des schémas à capture de choc de haute précision, il est donc indispensable de s'orienter vers l'utilisation de maillage adaptatif afin de pouvoir prendre en compte, par souci d'économie, les différents niveaux d'échelles des phénomènes et ce de façon dynamique.

        Dans cette présentation, nous nous attacherons à montrer la capacité d'un schéma à capture de choc de haute résolution [Daru and Tenaud (2004)] couplé à un algorithme de multirésolution « complètement » adaptatif [Cohen et al. (2003)], à reproduire les interactions fondamentales en mécanique des fluides. Les résultats portent sur des simulations numériques d'écoulements compressibles visqueux : advection d'un tourbillon, interaction entre une onde de choc faible et un spot de température ou un tourbillon, tube à choc. Les résultats sont analysés en terme drefficacité vis-à-vis de la précision de la solution mais également des gains en mémoire et temps CPU.

    .. section:: Un schéma de type volumes finis pour un modèle de croissance tissulaire.
        :class: description

        \A. Uzureau,  doctorant Laboratoire de Mathématiques de Nantes.

        L'os est le tissu le plus transplanté avec 1 million de greffes annuelles en Europe. La compréhension de la croissance tridimensionnelle du tissu osseux est un enjeu important. L'objectif final est de cultiver du tissu osseux ex vivo à partir de cellules souches mésenchymateuses du patient.
        On s'intéresse à un modèle de dynamique des populations qui décrit la prolifération, la migration et la différenciation des cellules souches ainsi que la synthèse de la matrice osseuse.
        La physique impose un principe du maximum (concentrations positives). La géométrie complexe de l'os contraint à utiliser un maillage non structuré. Pour répondre à ces deux contraintes, nous proposons de discrétiser ces équations par un schéma de type volumes finis tridimensionnel.


    .. section:: Influence de la géométrie du maillage sur le schéma de Godunov appliqué à l'équation des ondes dans le régime Bas Mach.
        :class: description

        \P. Omnes, CEA Saclay, service mécanique des fluides, modélisation et études et Université Paris 13, LAGA.

        Les schémas volumes finis de type Godunov sont utilisés dans de nombreux codes de mécanique des fluides. Il est connu que leur comportement est problématique lorsque le nombre de Mach de l'écoulement étudié est faible (apparition d'ondes de pression parasites). Il a été observé dans la littérature que centrer le gradient de pression et/ou utiliser des maillages triangulaires plutôt que quadrangulaires permettait d'améliorer les simulations.

        Dans cet exposé, nous analysons ces phénomènes sur le problème modèle de l'équation des ondes acoustiques. Il apparaît que la nature de l'espace stationnaire de l'équation des ondes discrètes joue un rôle central dans l'apparition d'ondes parasites. Dans le cas triangulaire et/ou gradient de pression centré, cet espace stationnaire est une bonne approximation de l'espace incompressible continu, alors que ce n'est pas le cas pour un maillage quadrangulaire avec les termes usuels de décentrement dans le gradient de pression. La diffusion numérique liée à ce décentrement transfère alors une partie de l'énergie incompressible vers l'espace acoustique, créant ainsi des ondes parasites, et ce d'autant plus rapidement que le nombre de Mach est faible.


    .. section:: Méthodes de raffinement hp pour l'équation de transport des neutrons.
        :class: description

        \D. Fournier, doctorant CEA Cadarache.

        L'équation de transport des neutrons en physique des réacteurs dépend des variables énergétiques, angulaires et spatiales. Elle permet de connaître la densité de neutrons à l'aide d'une grandeur appelée flux neutronique. Après discrétisation énergétique et angulaire, on se ramène à un système l'équations de transport-réaction. Ces équations sont couplées via le terme source ce qui rend la résolution du système coûteuse en temps et en espace mémoire. Afin d'améliorer ces deux aspects sans dégrader la convergence, des algorithmes adaptatifs ont été mis en place. Ces méthodes se basent sur une estimation locale de l'erreur afin de déterminer les zones où une meilleure précision est nécessaire.

        La discrétisation est réalisée par la méthode des éléments finis discontinus. Le raffinement peut ainsi être effectué en augmentant localement l'ordre de la base de polynômes (p−raffinement) ou en divisant une cellule (h−raffinement). On s'intéressera dans cette présentation à la combinaison de ces deux approches afin de mettre en place des algorithmes de hp−raffinement. Intuitivement, raffiner en p est plus avantageux lorsque la solution est régulière alors que le raffinement en espace doit être préféré pour les zones de non régularité. Il faut donc trouver un moyen d'estimer localement cette notion de régularité.

        La méthode présentée ici utilise deux estimateurs d'erreurs. Le premier suppose des hypothèses de régularité importante sur la solution [1] alors que le second requiert uniquement l'appartenance à l'espace des fonctions à variations bornées (BV) [2]. Dans les zones où les deux estimateurs sont équivalents, on peut conclure que la solution est régulière et privilégier le raffinement en ordre, et, dans le cas contraire, utiliser le raffinement en espace.

        Cette méthode sera testée et discutée sur des cas où l'on connaît la solution exacte, construite par la méthode des solutions manufacturées (MMS), et donc la régularité. Elle sera comparée à d'autres approches permettant de choisir entre h et p. Les comparaisons seront effectuées sur les cas construits par la MMS mais aussi sur des cas réalistes.

        - Références :

            [1] S. Adjerid and T. C. Massey. A posteriori discontinuous finite element error estimation for twodimensional hyperbolic problems. Comp. Meth. in Appl. Mech. and Eng., 191:5877–5897, 2002.

            [2] R. Eymard, T. Gallouët, M. Ghilani, and R. Herbin. Error estimates for the approximate solutions of a nonlinear hyperbolic equation given by finite volume schemes. Journal of Numerical Analysis, 18(4):563–594, 1998.}


    .. section:: Schémas volumes finis pour le problème de Stokes à viscosité variable
        :class: description

        \S. Krell, INRIA Lille.

        Lorsque la viscosité est variable, la résolution par volumes finis du problème de Stokes nécessite l'approximation du gradient de vitesse dans toutes les directions. Dans cet exposé, je présenterai des schémas de type DDFV (discrete duality finite volume) dont l'idée est d'introduire un jeu supplémentaire (par rapport à la méthode volumes finis standard) d'inconnues placées aux sommets du maillage. Une estimation d'erreur sera donnée et illustrée par des tests numériques. Pour finir, je montrerai qu'il est possible de prendre en compte de manière consistante d'éventuelles discontinuités de la viscosité.


    .. section:: Couplage des écoulements souterrains avec le ruissellement - Méthodologie numérique et applications.
        :class: description

        \P. Sochala, ingénieur de recherche BRGM Orléans.

        Une  modélisation des écoulements souterrains (en dimension deux) et leur couplage avec le ruissellement surfacique (en dimension une) est proposée. Les écoulements souterrains sont décrits  par  lʹéquation  de  Richards (discrétisée  par  une  méthode  BDF  en  temps  et  une méthode de Galerkine discontinue en espace) et les écoulements superficiels sont gouvernés par  le  système  de  Saint‐Venant  (discrétisé  par  une  méthode  de  Volumes  Finis).  Les  deux schémas,  pour  lʹécoulement  souterrain  et  pour  lʹécoulement  superficiel,  sont  conservatifs  et peuvent être utilisés dans des algorithmes de couplage faisant intervenir un ou plusieurs pas de temps. Pour assurer la conservation de la masse dʹeau totale du système couplé, les flux à lʹinterface doivent être convenablement choisis. Nous donnons en particulier la construction de  ces  flux  pour  les  schémas  BDF1  et  BDF2.  La  stratégie  de  couplage  proposée  permet  de traiter plusieurs mécanismes hydrologiques comme le ruissellement hortonien (ruissellement sur un sol sec), l'affleurement de nappe et le transfert d'eau entre différents sillons agricoles.


    .. section:: Développement de méthodes DDFV pour les équations d'Euler.
        :class: description

        \V. Desveaux,  doctorant Laboratoire de Mathématiques de Nantes.

        Nous nous intéressons à l'approximation numérique d'ordre élevé des équations d'Euler bidimensionnelles sur des maillages non structurés. Pour monter en ordre, nous utilisons des méthodes de type MUSCL. Par ailleurs, les techniques de reconstruction DDFV, récemment introduites pour l'approximation des problèmes elliptiques, permettent de reconstruire des gradients très précis grâce à l'utilisation de deux maillages duaux. L'utilisation des gradients DDFV dans les méthodes MUSCL nous permet d'obtenir un nouveau schéma performant. Nous porterons une attention particulière sur les procédures de limitation permettant d'assurer la  robustesse du schéma.


    .. section:: Tutoriel Galerkin Discontinu.
        :class: description

        \D. Le Roux, Institut Camille Jordan, Lyon.

        L'objectif de ce tutoriel est de présenter la méthode de Galerkin discontinue.

        Nous verrons tout d'abord, dans un contexte assez général, en quoi cette méthode s'apparente et se distingue des méthodes plus classiques d'éléments et de volumes finis.

        Nous considérerons aussi brièvement les étapes de son développement, en montrant comment cette méthode s'est peu à peu introduite dans la résolution de nombreux types d'équations (hyperboliques, elliptiques, paraboliques ...).

        Dans un deuxième temps nous analyserons la méthode de Galerkin discontinue du point de vue de la consistance, de la stabilité et de la convergence pour un certain nombre de problèmes.

        Nous tenterons de cerner les forces et les points faibles de la méthode par rapport aux méthodes plus classiques. Pour certains problèmes, en particulier les équations de Saint Venant, nous aurons recours à l'analyse de Fourier pour déterminer les modes propres des systèmes discrets.

        Enfin nous donnerons quelques résultats numériques appropriés.


    .. section:: Méthodes d'élements finis discontinus d'ordre élevé pour l'océan.
        :class: description

        \V. Legat, Ecole Polytechnique de Louvain.

        The first ocean general circulation models developed in the late sixties were based on finite differences schemes on structured grids. Many improvements in the fields of engineering have been achieved since three decades with the developments of new numerical methods based on unstructured meshes. New second generation models are now under study, with the aim of taking advantage of the potential of modern numerical techniques such as finite elements on unstructured grids. A popular new trend in engineering applications is the high- order Discontinuous Galerkin method, presenting many interesting numerical properties in terms of dispersion and dissipation, errors convergence rates, advection schemes, mesh adaptation. The key issue for unstructured high-order finite elements approach is the computational cost of the calculations. Therefore, we analyze three routes to have new efficient schemes:

        (1) Exploit single precision BLAS/LAPACK for the efficient implementation of the explicit and implicit discontinuous Galerkin methods.

        (2) Use novel time-integration procedures for multi-scale models and adpat the time-integration scheme to the physical processes.

        (3) Introduce multi-level methods for the implicit linear and non-linear solvers with multigrid methods as a preconditioner for stiff, non-linear and non-positive-definite systems. Each route could reduce the computational cost by one order of magnitude.


    .. section:: Etude d'une méthode Galerkin discontinue en maillages hybrides pour la résolution numérique des équations de Maxwell instationnaires.
        :class: description

        \C. Durochat, doctorant INRIA Sophia Antipolis.

        L'objectif de cette étude est d'analyser et valider une méthode Galerkin discontinue pour résoudre numériquement les équations de Maxwell instationnaires sur des maillages hybrides tétraédriques / hexaédriques en 3D (triangulaires / quadrangulaires en 2D), que l'on appelle méthode GDDT-Pp/Qk.
        Comme dans di-fférents travaux déjà réalisés sur plusieurs méthodes hybrides, notre motivation principale est de mailler des objets ayant une géométrie complexe à l'aide de tétraèdres afin d'obtenir une précision optimale, et de mailler le reste du domaine (le vide environnant) à l'aide d'hexaèdres impliquant un gain en terme de mémoire et de temps de calcul.

        Dans la méthode GD considérée, pour approximer le champ  électromagnétique, nous formulons les schémas de discrétisation spatiale en trois dimensions (3D), basée sur une interpolation polynomiale nodale d'ordre arbitraire. Nous utilisons un fl ux centré pour approcher les intégrales de surface et un schéma d'intégration en temps de type saute-mouton d'ordre deux ou quatre. L'étude de stabilité L2 (3D également) de cette méthode montre qu'elle conserve une énergie discrète et permet d'exhiber une condition suffisante de stabilité de type CFL. Le résultat alors obtenu est qu'il suffit que le pas de temps global soit égal au minimum entre  le pas de temps sur la partie tétraédrique et celui sur la partie hexaédrique. Dans un travail en cours, nous analysons la convergence de la méthode, afin de dégager un estimateur d'erreur a priori.

        Nous complétons enfin cette étude par la simulation de di-fférents cas tests numériques en deux dimensions d'espace (ondes TMz) : l'évolution d'un mode dans une cavité métallique carrée, dans une cavité métallique circulaire, ainsi que la diff-raction d'une onde plane par un cylindre PEC. Les résultats des tests sont réalisés pour p = 0, ..., 4 et k = 0, ..., 4 et pour des maillages conformes, où nous pourrons commencer à percevoir les compromis précision/temps CPU que ces hybridations Pp/Qk peuvent offrir.



    .. section:: Une nouvelle méthode cut-cell pour la simulation numérique d'écoulements bidimensionnels autour d'obstacles.
        :class: description

        \N. James, post-doctorant EPFL.

        Nous présentons une nouvelle méthode du second ordre, basée sur le schéma MAC sur grilles cartésiennes, pour la simulation numérique d'écoulements incompressibles autour d'obstacles en deux dimensions d'espace.

        Dans cette approche, l'interface solide/liquide est projetée sur un maillage cartésien. Les discrétisations des termes visqueux et convectifs sont formulées dans le cadre des méthodes volumes finis afin d'assurer au schéma des propriétés de conservation.

        Des approximations centrées d'ordre deux sont utilisées dans les mailles qui sont suffisamment loin de l'obstacle. Dans les mailles coupées par l'obstacle, des approximations du premier ordre sont proposées.

        Les systèmes linéaires associés ne sont pas symétriques mais diffèrent peu de ceux qui correspondent au schéma MAC usuel (sans obstacle). Ils sont résolus de façon originale par une méthode directe basée sur une méthode appelée capacitance matrix method.

        La discrétisation temporelle est confiée a une méthode de projection du second ordre. Alors que dans les cellules coupées l'approximation est localement du premier ordre, la schéma obtenu est globalement d'ordre deux.

        La résolution du problème de Taylor-Couette, dont une solution analytique est connue, permet de montrer la précision de la méthode. L'efficacité et la robustesse de la méthode sont illustrées par la simulation numérique d'écoulements bidimensionnels autour d'un cylindre pour des nombres de Reynolds allant jusqu'à 9500.

        On observe un bon accord avec les données expérimentales et les autres résultats numériques publiés dans la Littérature.


    .. section:: Méthodes numériques d'homogénéisation pour des problèmes elliptiques non-linéaires non-monotones.
        :class: description

        \G. Vilmart, Ecole Polytechnique Fédérale de Lausanne.

        On analyse l'effet de l'intégration numérique dans la méthode des éléments finis pour une classe de problèmes elliptiques non-linéaires de type non-monotone.
        Ces équations interviennent dans de nombreux modèles, comme des problèmes d'infiltration d'eau dans des milieux poreux, des problèmes de potentiel électrique ou de diffusion de la chaleur dans des matériaux.

        Nous examinons d'abord le cas d'une seule échelle. Sous des hypothèses similaires au cas linéaire sur les formules de quadrature, on démontre les estimations d'erreur a priori pour les normes $L2$ et $H1$, ainsi que l'unicité de la solution numérique pour un maillage suffisamment fin et on discute la convergence de la méthode de Newton.

        On considère ensuite une classe de problèmes non-linéaires multi-échelles d'homogénéisation (deux échelles spatiales), pour lesquels le coût de la méthode des éléments finis standard devient prohibitif.
        Nous analysons la Finite Element Heterogeneous Multiscale Method (FE-HMM) qui est basée sur la méthode des éléments finis avec quadrature numérique et repose sur un couplage de méthodes appliquées aux échelles macroscopiques et microscopiques.

        Il s'agit d'un travail en collaboration avec Assyr Abdulle.


    .. section:: Méthode de Galerkin discontinues pour les écoulements multiphasiques compressibles
        :class: description

        \V. Perrier, INRIA Bordeaux Sud Ouest.

        Le but de cet exposé est de développer une méthode numérique d'ordre élevé pour des modèles d'écoulements multiphasiques compressibles. Nous aborderons en particulier les difficultés liées à l'aspect non conservatifs et non linéaire de ces systèmes.


    .. section:: Estimateurs d'erreur a posteriori pour les équations de la magnétodynamique en formulation potentielle (A/φ) harmonique
        :class: description

        \Z. Tang, doctorant Laboratoire Paul Painlevé, Lille.

        On considère dans ce travail les équations de Maxwell dans leur formulation en potentiel dite "A/φ" et en régime harmonique, résolues à l'aide d'une méthode de type éléments finis. Une fois établi les caractères bien posés des problèmes continu et discret, on s'intéresse à l'obtention d'un estimateur d'erreur a posteriori résiduel, permettant de majorer l'erreur globale (fiabilité) et de minorer l'erreur locale (efficacité) à l'aide de quantités ne dépendant que des données et de la solution numérique. Plusieurs outils analytiques nécessaires à cette approche sont devéloppés, dont en particulier une décomposition de Helmholtz adaptée au problème. Des simulations numériques sur des cas tests caractéristiques sont menés avec le logiciel

        Code Carmel3D: Ce code de calcul, developpé par le laboratoire commun LAMEL qui regroupe l'équipe modélisation du L2EP et le groupe machines électriques de EDF, vise à simuler des phénomènes régis par les équations de Maxwell en régime quasi-stationnaire (ARQS). L'estimateur d'erreur proposé dans ce travail se révèle pertinent tant pour le contrôle de l'erreur globale que l'analyse de l'erreur locale permettant de mener à bien des stratégies de raffinement adaptatif de maillage.
