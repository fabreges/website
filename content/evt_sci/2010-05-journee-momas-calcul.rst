Journée GNR MOMAS / GDR Calcul
##############################

:date: 2010-02-19 12:15:34
:modified: 2010-02-19 12:15:34
:category: journee
:tags: momas
:start_date: 2010-05-05
:end_date: 2010-05-05
:place: Paris
:summary: Le GNR Momas et le GdR Calcul ont organisé une journée commune sur le thème calcul haute performance et applications au stockage souterrain des déchets.

.. contents::

.. section:: Description
    :class: description

    Le GNR Momas et le `GDR Calcul <index.html>`_ ont organisé une journée commune sur le thème calcul haute performance et applications au stockage souterrain des déchets.

    Les conférenciers ont été invités à la fois par le GDR Calcul, et par le GNR MOMAS. Le but de cette journée était de favoriser les échanges entre les deux communautés en apportant des éclairages différents sur les enjeux, les méthodes et les algorithmes utilisés en HPC.


.. section:: Programme
    :class: programme

    .. schedule::

        .. day:: 05-05-2010

            .. event:: Développements pour les écoulements et le transport en milieux poreux dans le code MPCube : mise en oeuvre dans un environnement parallèle et EF/VF multi-échelles
                :begin: 09:00
                :end: 09:45
                :speaker: Philippe Montarnal, co-auteurs Th. Abballe, F. Caro et E. Laucoin
                :support: attachments/spip/Documents/Journees/mai2010/PMontarnal.pdf

            .. event:: Méthodes spectrales, grilles creuses, et analyses multirésolution pour la propagation des incertitudes
                :begin: 09:45
                :end: 10:30
                :speaker: Oliver Le Maitre
                :support: attachments/spip/Documents/Journees/mai2010/OLeMaitre.pdf

            .. break_event:: Pause
                :begin: 10:30
                :end: 10:45


            .. event:: Les calculs hautes performances à EDF autour des codes utilisés pour le stockage
                :begin: 10:45
                :end: 11:30
                :speaker: Sylvie Granet
                :support: attachments/spip/Documents/Journees/mai2010/SGranet.pdf


            .. event:: Le calcul haute performance à l'Andra : quelques exemples de réalisations à ce jour
                :begin: 11:30
                :end: 12:15
                :speaker: Marc Leconte et Bernard Vialay
                :support: attachments/spip/Documents/Journees/mai2010/BVialay-MLeconte.pdf


            .. break_event:: Déjeuner
                :begin: 12:15
                :end: 14:00


            .. event:: Solveurs linéaires parallèles par décomposition de domaine algébrique
                :begin: 14:00
                :end: 14:45
                :speaker: Luc Giraud
                :support: attachments/spip/Documents/Journees/mai2010/LGiraud.pdf


            .. event:: Méthodes de raffinement espace-temps
                :begin: 14:45
                :end: 15:30
                :speaker: Caroline Japhet
                :support: attachments/spip/Documents/Journees/mai2010/CJaphet.pdf


            .. event:: Méthodes numériques performantes en espace/temps pour la simulation des fluides réactifs
                :begin: 15:30
                :end: 16:15
                :speaker: Stéphane Descombes
                :support: attachments/spip/Documents/Journees/mai2010/SDescombes.pdf


            .. break_event:: Pause
                :begin: 16:15
                :end: 16:30


            .. event:: Méthodes numériques pour des modèles couplés et stochastiques d'hydrogéologie
                :begin: 16:30
                :end: 17:15
                :speaker: Jocelyne Erhel
                :support: attachments/spip/Documents/Journees/mai2010/JErhel.pdf


            .. event:: Le projet BigDFT : méthodes Ab initio, ondelettes et supercalculateurs massivement parallèles avec GPU
                :begin: 17:15
                :end: 18:00
                :speaker: Luigi Genovese
                :support: attachments/spip/Documents/Journees/mai2010/LGenovese.pdf



.. section:: Intervenants
    :class: orga

    - Stéphane Descombes (Laboratoire J.Dieudonné)
    - Jocelyne Erhel (IRISA)
    - Luigi Genovese (ESRF, prix BULL-Fourier 2009)
    - Luc Giraud (INRIA)
    - Sylvie Granet, (EDF Clamart)
    - Caroline Japhet (Paris 13)
    - Marc Leconte (ANDRA)
    - Olivier Le Maitre (LIMSI)
    - Philippe Montarnal (CEA Saclay)
    - Bernard Vialay (ANDRA)


.. section:: Organisateurs
    :class: orga

    - Grégoire Allaire
    - Stéphane Lantéri
    - Violaine Louvet
