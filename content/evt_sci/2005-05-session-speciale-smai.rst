Congrès SMAI 2005 : session spéciale
####################################

:date: 2008-02-12 13:18:33
:modified: 2008-02-12 13:18:33
:category: journee
:tags: smai
:place: Evian
:start_date: 2005-05-23
:end_date: 2005-05-27
:summary: La session spéciale du Groupe Calcul au congrès SMAI 2005 qui a eu lieu du 23 au 27 mai 2005 à Evian a eu pour thème le calcul sur grille.


.. contents::

.. section:: Description
    :class: description

    La session spéciale du Groupe Calcul au congrès SMAI 2005 qui a eu lieu du 23 au 27 mai 2005 à Evian a eu pour thème le calcul sur grille.

    Cette session a consisté en :

    - trois présentations sur l'environnement de calcul sur grille :

        - **Caron Eddy** : `DIET, un environnement Grid-RPC <attachments/spip/Documents/Manifestations/SMAI2005/diet.pdf>`__
        - **Desprez Frédéric** : `Tour d'horizon sur les grilles de calcul et de données <attachments/spip/Documents/Manifestations/SMAI2005/grilles.pdf>`__
        - **Fedak Gilles** : `Le projet XtremWeb INRIA Futurs <attachments/spip/Documents/Manifestations/SMAI2005/xtremweb.pdf>`__

    - une présentation sur l'utilisation des grilles de calcul :

        - **Mesri Youssef** : `Partitionnement de maillages sur une grille de calcul <attachments/spip/Documents/Manifestations/SMAI2005/maillages_grille.pdf>`__
 
    - un exposé/débat sur le projet CIEL :

        - **Louvet Violaine** : `Codes Informatique En Ligne : présentation et discussion <attachments/spip/Documents/Manifestations/SMAI2005/ciel-smai2005.pdf>`__

