{% if header %}
<div class="cluster">
    <div class="card">
        <div class="card-header">
            <h6>
                {{ cluster['name'] }}
            </h6>
        </div>
        <div class="card-body row">
            {% if cluster['clustercorenumber'] %}
                <p class="col-sm-3"><img src="../theme/img/cpu.png" alt="Nombre de cœurs" title="Nombre de cœurs" />
                    {{ cluster['clustercorenumber'] }} cœurs
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/cpu.png" alt="Nombre de cœurs" title="Nombre de cœurs" />
                </p>
            {% endif %}
            {% if cluster['clusterramsize'] %}
                <p class="col-sm-3"><img src="../theme/img/memory.png" alt="RAM" title="RAM" />
                    {{ cluster['clusterramsize'] | format_storage(3)}}
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/memory.png" alt="RAM" title="RAM" />
                </p>
            {% endif %}
            {% if cluster['clustergpunumber'] %}
                <p class="col-sm-3"><img src="../theme/img/gpu.png" alt="Nombre de GPUs" title="Nombre de GPUs" />
                    {{ cluster['clustergpunumber'] }} GPUs
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/gpu.png" alt="Nombre de GPUs" title="Nombre de GPUs" />
                </p>
            {% endif %}
            {% if cluster['jobschedulername'] %}
                <p class="col-sm-3"><img src="../theme/img/scheduler.png" alt="Gestionnaire de tâches" title="Gestionnaire de tâches" />
                    {{ cluster['jobschedulername'] }}
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/scheduler.png" alt="Gestionnaire de tâches" title="Gestionnaire de tâches" />
                </p>
            {% endif %}
        </div>
        <div class="card-body row">
            {% if cluster['networktype'] %}
                <p class="col-sm-3"><img src="../theme/img/network.png" alt="Type de réseau" title="Type de réseau" />
                    {{ cluster['networktype'] }}
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/network.png" alt="Type de réseau" title="Type de réseau" />
                </p>
            {% endif %}
            {% if cluster['networkbandwidth'] %}
                <p class="col-sm-3"><img src="../theme/img/bandwidth.png" alt="Bande passante réseau" title="Bande passante réseau" />
                    {{ cluster['networkbandwidth'] }} Gb/s
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/bandwidth.png" alt="Bande passante réseau" title="Bande passante réseau" />
                </p>
            {% endif %}
            {% if cluster['networktopology'] %}
                <p class="col-sm-3"><img src="../theme/img/topology.png" alt="Topologie réseau" title="Topologie réseau" />
                    {{ cluster['networktopology'] }}
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/topology.png" alt="Topologie réseau" title="Topologie réseau" />
                </p>
            {% endif %}
            {% if cluster['vendorname'] %}
                <p class="col-sm-3"><img src="../theme/img/brand.png" alt="Constructeur" title="Constructeur"/>
                    {{ cluster['vendorname'] | join_names }}
                </p>
            {% else %}
                <p class="col-sm-3"><img src="../theme/img/brand.png" alt="Constructeur" title="Constructeur"/>
                </p>
            {% endif %}
        </div>
        <div class="card-footer row">
        </div>
{% endif %}
{% if footer %}
    </div>
</div>
{% endif %}
