{% if header %}
<div class="meso_list">
  <div class="container">
    <div class="panel-group">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h6 class="panel-title">
            <a data-toggle="collapse" href="#collapse{{ num }}">Clusters</a>
          </h6>
        </div>
        <div id="collapse{{ num }}" class="panel-collapse collapse">
          <ul class="list-group">
{% endif %}
{% if footer %}
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
{% endif %}
