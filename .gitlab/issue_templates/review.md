- category: paysage/groupe/evt_sci/evt_tech ?
- tags: journée/formation ?

Les tâches de vérification :

* [ ] Vérifier dans le fichier `content/spip2pelican.log` si des erreurs ont été détectées lors de la conversion avec `spip2pelican`
* [ ] Corriger l'orthographe
* [ ] Corriger les erreurs d'encodage
* [ ] Corriger la mise en forme
* [ ] Corriger la pertinence du contenu
* [ ] Mettre en adéquation avec la RGPD
* [ ] Mettre un tag journée ou un tag formation ?

Tant qu'on n'a pas une solution automatique :

* [ ] Vérifier les liens internes
* [ ] Vérifier les liens externes

/label ~Transfert
/milestone %"Tester le workflow de review"