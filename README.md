# Construction du site <https://calcul.math.unistra.fr>

## Installation de Pelican et prérequis

### Git-lfs

[`git-lfs`](https://git-lfs.github.com/
) est utilisé pour gérer les fichiers attachés.

### Paquets python

Il est conseillé d'utiliser [`virtualenv`](https://virtualenv.pypa.io/en/latest/userguide/) ou [`conda-env`](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) pour isoler vos installations python.

#### Avec `virtualenv`

On crée un virtualenv dans la racine du projet

```bash
virtualenv --python=python3 venv
```

On l'active

```bash
source venv/bin/activate
```

On install les dépendances et Pelican avec

```bash
pip install -r requirements.txt
```

#### Avec `conda-env`

On crée un conda-env qui contient les dépendances

```bash
conda env create --file environment.yml
```

On l'active

```bash
conda activate calcul
```

#### Avec Docker

On se connecte au registry :

```bash
docker login registry.math.unistra.fr
```

On récupère l'image docker :

```bash
docker pull registry.math.unistra.fr/groupe-calcul/website
```

On instancie l'image en montant le répertoire courant qui doit correspondre à la racine du projet :

```bash
docker run --rm -ti -v $PWD:/home/calcul/website \
registry.math.unistra.fr/groupe-calcul/website
```

> Plus d'info dans la [doc gitlab](/container_registry).

## Construction du site

Pour visualiser en local le rendu du site :

```bash
make devserver
```

puis se connecter sur <http://localhost:8000>.
La modification d'un fichier source markdown est détectée dynamiquement par le script qui relance automatiquement la construction du site pour mettre à jour le rendu.

Le numéro du port de publication peut-être changé avec

```bash
make devserver PORT=8001
```

pour publier sur <http://localhost:8001>.

## Publication

La publication se fait par intégration continue avec GitLab-CI.
Tout est décrit par le fichier [.gitlab-ci.yml](.gitlab-ci.yml).
En combinaison avec la configuration apache de `calcul.math.unistra.fr`, le fichier `.gitlab-ci.yml` permet de publier sur une adresse qui dépend de la branche git :

- `master` publie vers <https://calcul.math.cnrs.fr> en utilisant le service [PLMShift](http://plmshift.pages.math.cnrs.fr) de Mathrice
- les branches `dev-*` publient vers <https://calcul-dev.math.unistra.fr/dev-*>
- Toute autre `branche` listée dans le champ `apache-dev::only:` publie également vers <https://calcul-dev.math.unistra.fr/branche>
- Toute autre branche non listée est testée en construction par le job `build` mais n'est pas publiée

## Note sur la préparation des vidéos 

### Extraction depuis les bbb

L'extraction de la vidéo enregistrée par bbb peut être faite à partir des scripts de Roland, disponibles ici :

`git clone https://github.com/rolanddenis/bbb-recorder.git`

Dans le fichier `export.js`, modifier éventuellement le chemin d'extraction de la vidéo en fonction de votre environnement 

-const downloaddir = homedir + "/Téléchargements";
+const downloaddir = homedir + "/Downloads";

### Récupérer le format webm

Le scripts de `bbb-recorder` récupère un fichier webm

`node export.js "https://BBB-HOST/playback/presentation/2.0/playback.html?meetingId=MEETING-ID" meeting.webm 0`

### Troncature éventuelle du début et de la fin du fichier video

L'extraction du bbb donne un fichier `meeting.webm` qui peut être directement téléversé sur le site de `canal-u.tv`. Il peut être nécessaire de modifier ce fichier au préalable, notamment pour en couper le début et la fin. Par exemple,  pour supprimer les `00:01:20` premières secondes et poursuivre le film pendant 1h05 et 32s (3932 secondes), sans perte de qualité :

`ffmpeg -ss 00:01:20 -t 3932 -i meeting.webm -c:v copy -c:a copy cut.mkv`

La conversion du mkv en mp4 peut être faite ensuite via 

`ffmpeg -i cut.mkv -c:v copy -c:a copy -strict -2 movie.mp4`

### Dépôt dans canal-u.tv

Pour déposer le fichier sur `canal-u.tv` pour le Groupe Calcul, il est nécessaire d'avoir un compte administrateur 

`https://www.canal-u.tv/admin/`

Pour déposer un fichier, aller dans __Programme__

`https://www.canal-u.tv/admin/index.php?curTable=c_programme`

et cliquer sur __Ajouter un élément__ 

`https://www.canal-u.tv/admin/index.php?curTable=c_programme&curId=new`

Il est demandé d'importer un fichier que le site canal-u.tv réencode en `mp4` après téléversement (attendre le message de confirmation du réencodage pour basculer la vidéo en mode visible à tous).


