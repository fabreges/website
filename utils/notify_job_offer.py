#!/usr/bin/env python3
"""Email confirmation to job offer author"""

import argparse
import smtplib
from email.message import EmailMessage
from email.headerregistry import Address
from string import Template
import getpass
import os
import re
import sys
import datetime
import locale
locale.setlocale(locale.LC_TIME, '')
script_path = os.path.dirname(sys.argv[0])
sys.path.append(os.path.join(script_path, '..'))
from publishconf import SITEURL
import html


# Email
MAILSERVER = 'mailserver.u-strasbg.fr'
SENDER_ACCOUNT = 'math-gitlab-incoming'
SENDER_MAIL = "calcul-contact@services.cnrs.fr"
SENDER_NAME = "Bureau du groupe Calcul"
SENDER_ACCOUNT_DISPLAY = "calcul-contact"
SENDER_DOMAIN = "math.cnrs.fr"
RECIPIENT_MAIL = "bureau.calcul@services.cnrs.fr"


class Message:
    """Abstract class for message"""

    subject = ""
    template = Template("")

    def __init__(self, job_id, recipient_email=None, publisher=None):
        self.job_id = job_id
        self.parse_job_offer_file()
        self.recipient_name = None
        self.recipient_email = recipient_email

        job_url = "{}/job_{}.html".format(SITEURL, self.job_id)
        self.d = {'JOB_URL': job_url}
        if publisher:
            self.d['SIGNATURE'] = "{}, pour le bureau du groupe Calcul".format(publisher.title())
        else:
            self.d['SIGNATURE'] = 'Le bureau du groupe Calcul'


    def parse_job_offer_file(self):
        filename = f"../content/job_offers/job_{self.job_id}.md"
        filepath = os.path.join(script_path, filename)

        with open(filepath) as f:
            s = f.read()
            m = re.search(r"^Authors: (.*)$", s, re.MULTILINE)
            self.author_name = html.unescape(m.group(1)).title()
            m = re.search(r"^Email: (.*)$", s, re.MULTILINE)
            self.author_email = html.unescape(m.group(1))
            m = re.search(r"^Job_Type: (.*)$", s, re.MULTILINE)
            self.job_type = html.unescape(m.group(1)).lower()
            m = re.search(r"^Job_Duration: (.*)$", s, re.MULTILINE)
            self.job_duration = html.unescape(m.group(1)).lower()
            m = re.search(r"^Title: (.*)$", s, re.MULTILINE)
            self.job_title = html.unescape(m.group(1))
            m = re.search(r"^Expiration_Date: (.*)$", s, re.MULTILINE)
            if m:
                self.expire_date = datetime.datetime.strptime(m.group(1), '%Y-%m-%d')
            else:
                self.expire_date = datetime.date.today() + datetime.timedelta(days=90)

    def get_email_body(self):
        """Virtual function"""
        pass

    def get_msg(self):
        """Get a message object for job_id"""
        body = self.get_email_body()
        msg = EmailMessage()
        msg['Subject'] = self.subject
        msg['From'] = Address(SENDER_NAME, SENDER_ACCOUNT_DISPLAY, SENDER_DOMAIN)
        msg['To'] = self.recipient_email

        print(f">>> Sending notification to {self.recipient_name} <{self.recipient_email}>")
        print(body)
        msg.set_content(body)

        return msg


class AuthorMessage(Message):
    """A class to notify job offer authors"""

    subject = "Votre annonce d'offre d'emploi a été validée"
    template = Template("""\
Bonjour,

Merci d'avoir déposé une annonce d'offre d'emploi sur le site web du groupe Calcul.
Cette annonce a été validée par le bureau du groupe Calcul et vient d'être :
  - publiée sur $JOB_URL
  - diffusée sur la liste calcul@listes.math.cnrs.fr.

Sans demande de votre part, elle restera en ligne jusqu'au $EXPIRE_DATE.

Cordialement,

$SIGNATURE
""")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.recipient_name = self.author_name
        if not self.recipient_email:
            self.recipient_email = self.author_email

    def get_email_body(self):
        """Return email body from template"""

        self.d['EXPIRE_DATE'] = '{d.day} {d:%B} {d.year}'.format(d=self.expire_date)
        body = self.template.substitute(self.d)
        return body


class ListMessage(Message):
    """A class to notify diffusion list"""

    template = Template("""\
Bonjour,

$JOB_AUTHOR vient de publier une offre de $JOB_TYPE$JOB_DURATION concernant le poste intitulé "$JOB_TITLE".

Vous en retrouverez tous les détails sur $JOB_URL

Cordialement,

$SIGNATURE
""")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.recipient_name = "Groupe Calcul"
        if not self.recipient_email:
            self.recipient_email = RECIPIENT_MAIL
        self.subject = "Offre de {}{}: {}".format(
            self.job_type,
            " de {}".format(self.job_duration) if self.job_duration else "",
            self.job_title
        )

    def get_email_body(self):
        """Return email body from template"""

        self.d['JOB_AUTHOR'] = self.author_name
        self.d['JOB_TYPE'] = self.job_type
        self.d['JOB_TITLE'] = self.job_title
        self.d['JOB_DURATION'] = " ({})".format(self.job_duration) if self.job_duration else ""

        body = self.template.substitute(self.d)

        return body


def send_email(commit_title=None, job_id=None, password=None, notifier=None, recipient_email=None, publisher=None):
    """Send notification email using smtplib"""

    if not job_id:
        m = re.match("^Merge branch 'job_(.*)' into '(.*)'$", commit_title)
        job_id = m.group(1)

    message = notifier(job_id, recipient_email, publisher)
    msg = message.get_msg()

    if not password:
        password = getpass.getpass(f"Please type email password for {SENDER_MAIL}: ")

    # Connect to SMTP
    with smtplib.SMTP_SSL(MAILSERVER) as smtp:
        smtp.login(SENDER_ACCOUNT, password)
        smtp.sendmail(SENDER_MAIL, msg['To'], msg.as_string())


def main():
    """Read CLI to send email"""
    parser = argparse.ArgumentParser(description="Send email notification to the author of job offer")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--commit_title', nargs=1, metavar='commit-title',
                       help='should be "$CI_COMMIT_TITLE" in .gitlab-ci.yml')
    group.add_argument('--job_id', nargs=1, metavar='job-id',
                       help="bf41a2147c42ef38c98a2cc47b244012 for example")
    parser.add_argument('--password', nargs=1, metavar="account-password", help="password for sender email account")
    parser.add_argument('--notifier', nargs=1, metavar="notifier-type", required=True, choices=("author", "list"),
                        help="Type for notifying message")
    parser.add_argument('--recipient_email', nargs=1, metavar="recipent-email-address",
                        help="Override default (author or list address)")
    parser.add_argument('--publisher', nargs=1, metavar="publish-name",
                        help="Name of Groupe Calcul's member responsible for publishing the job offer.")
    args = parser.parse_args()

    password = args.password[0] if args.password else None
    notifier = {"author": AuthorMessage, "list": ListMessage}
    recipient_email = args.recipient_email[0] if args.recipient_email else None
    publisher = args.publisher[0] if args.publisher else None
    if args.commit_title:
        send_email(commit_title=args.commit_title[0], password=password, notifier=notifier[args.notifier[0]],
                   recipient_email=recipient_email, publisher=publisher)
    elif args.job_id:
        send_email(job_id=args.job_id[0], password=password, notifier=notifier[args.notifier[0]],
                   recipient_email=recipient_email, publisher=publisher)


if __name__ == '__main__':
    main()
