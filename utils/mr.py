import argparse
import gitlab

day_before_prod_date = '2019-06-16'


def connect_to_gitlab(gitlab_id=None):
    """Return a connection to GitLab API"""
    try:
        gl = gitlab.Gitlab.from_config(gitlab_id)
    except (gitlab.config.GitlabIDError, gitlab.config.GitlabDataError,
            gitlab.config.GitlabConfigMissingError) as e:
        print("Exception in python-gitlab: {}.\n".format(e),
              "Check python-gitlab configuration on",
              "http://python-gitlab.readthedocs.io/en/stable/cli.html",
              file=sys.stderr)
        sys.exit(1)

    return gl


def get_mrs(project_id):
    """Get all MRs from project_id"""
    gl = connect_to_gitlab()
    project = gl.projects.get(project_id)
    mrs = project.mergerequests.list(state='merged', order_by='updated_at',
                                     all=True)
    return mrs


def main(date_start=day_before_prod_date):
    mrs = get_mrs(project_id=309)
    # List MRs after production date
    mrs_prod = [mr for mr in mrs if (mr.merged_at > date_start and
                                     mr.target_branch == 'master')]
    print(f"{len(mrs_prod)} merged MRs after {date_start}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Count MR since date')
    parser.add_argument(
        '--date', '-d', dest='date_start', type=str,
        default=day_before_prod_date,
        help='Date of first MR (default is {})'.format(day_before_prod_date))
    args = parser.parse_args()
    main(args.date_start)
