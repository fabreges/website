#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Report security updates and reboot required using saltstack
"""
import argparse
import gitlab
import glob
import re
import subprocess
import sys
import os
from pelican.utils import slugify


GITLAB_PROJECT_ID = 309  # Project id that holds the target issue
GITLAB_ISSUE_ID = 49  # Target issue id
GITLAB_URL = 'https://gitlab.math.unistra.fr'
TOCLEAN_DIR = "content_not_clean_yet"
CLEANED_DIR = "content"


def connect_to_gitlab(gitlab_url=GITLAB_URL):
    """Return a connection to GitLab API"""
    try:
        gitlab_private_token = os.environ['GITLAB_PRIVATE_TOKEN']
    except KeyError:
        print("GITLAB_PRIVATE_TOKEN environment variable not set!")
        sys.exit(1)

    try:
        gl = gitlab.Gitlab(gitlab_url, private_token=gitlab_private_token)
    except (gitlab.config.GitlabIDError, gitlab.config.GitlabDataError) as e:
        print("Exception in python-gitlab: {}.\n".format(e),
              "Check python-gitlab configuration on",
              "http://python-gitlab.readthedocs.io/en/stable/cli.html",
              file=sys.stderr)
        sys.exit(1)

    return gl


def parse_rst_files():
    """walk into TOCLEAN_DIR for .rst files and return a list of file info dictionaries"""
    file_paths = glob.glob(TOCLEAN_DIR + '/**/*.rst', recursive=True)
    file_paths.extend(glob.glob(CLEANED_DIR + '/**/*.rst', recursive=True))

    date_file = []
    files = []
    for file_path in file_paths:
        cleaned = re.match(r"^{}/".format(CLEANED_DIR), file_path) is not None
        content = open(file_path).read()
        year = re.search(r"^:date: ([0-9]+)", content, re.M).group(1)
        date_file.append((year, file_path))

        try:
            # First rst syntax for title: use metadata directive
            title = re.search(r"^:title: (.*)", content, re.M).group(1)
        except AttributeError:
            # Second rst syntax for title: read first line
            title = content.split("\n")[0]

        try:
            slug = re.search(r"^:slug: (.*)", content, re.M).group(1)
        except AttributeError:
            filename = os.path.splitext(os.path.basename(file_path))[0]
            slug = slugify(filename)

        files.append({'year': year, 'path': file_path, 'title': title, 'cleaned': cleaned, 'slug': slug})

    return files


def get_popularities():
    """Open popularite.txt and return a {filepath: popularite} dict"""
    with open("utils/popularite.txt", 'r') as f:
        popularite = {}
        for line in f:
            path, score = line.strip().split()
            popularite.update({path: float(score)})
        return popularite


def main(update_gitlab_issue):
    """Print to standard output or update gitlab issue"""

    files = parse_rst_files()
    years = sorted(list(set([file['year'] for file in files])))

    popu = get_popularities()
    body = ""
    for year in years:
        body += "\n**{} :**\n\n".format(year)
        lines = []
        for file in files:
            if file['year'] == year:
                path = re.match(r"^({}|{})/(.*)$".format(TOCLEAN_DIR, CLEANED_DIR), file['path']).group(2)
                if file['cleaned']:
                    mdpath = "[`{}`](https://calcul-dev.math.unistra.fr/develop/{}.html)".format(path, file['slug'])
                    line = "* [x] {}: {}".format(mdpath, file['title'])
                else:
                    line = "* [ ] `{}`: {}".format(path, file['title'])

                popularite = popu.get(path, 0)
                if popularite:
                    line += " [{}]\n".format(popularite)
                else:
                    line += "\n"
                lines.append(line)

        body += ''.join(sorted(lines))

    if update_gitlab_issue and body:

        new_description = body

        gl = connect_to_gitlab()
        project = gl.projects.get(GITLAB_PROJECT_ID)
        issue = project.issues.get(GITLAB_ISSUE_ID)
        if new_description != issue.description:
            issue.state_event = 'reopen'
            issue.labels = ['Transfert']
            issue.milestone = 'Transfert du contenu spip'
            issue.description = new_description

            # mention last commit hash in issue note
            cmd = "git log -1 --pretty=format:%h".split()
            result = subprocess.run(cmd, stdout=subprocess.PIPE)
            last_commit_hash = result.stdout.decode('utf-8')
            issue_note_body = "#{} mise à jour par {}".format(GITLAB_ISSUE_ID, last_commit_hash)

            # acknowledge last committer using @mention in issue note
            cmd = "git log -1 --pretty=format:%an".split()
            result = subprocess.run(cmd, stdout=subprocess.PIPE)
            author_name = result.stdout.decode('utf-8')
            matching_users = gl.users.list(search=author_name, all=True)
            if matching_users:
                username = matching_users[0].username
                issue_note_body += ", merci @{} :kissing_heart: !".format(username)

            issue.notes.create({'body': issue_note_body})
            issue.save()
        else:
            print("Issue description does not require any update!")

    else:
        print(body)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
             description="Sort articles to be cleaned after Spip -> Pelican conversion")
    parser.add_argument('--issue', dest='update_gitlab_issue', action='store_true', default=False,
                        help="Update GitLab issue")
    args = parser.parse_args()

    main(args.update_gitlab_issue)
